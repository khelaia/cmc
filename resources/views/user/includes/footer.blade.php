<footer id="footer_fluid">
    <div class="footer_container">
        <div class="footer_menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="footer_company">
                            <div class="footer_company_title">
                                <h3>COMPANY</h3>
                            </div>
                            <div class="footer_company_menu">
                                <ul>
                                    @php $contact = \App\Contact::findOrFail(1); @endphp
                                    <li><a href=""><img src="/assets/images/flocation.png" alt="">{{$contact->{'address_'.$locale} }}</a></li>
                                    <li><a href=""><img src="/assets/images/fmsg.png" alt="">{{$contact->{'email'} }}</a></li>
                                    <li><a href=""><img src="/assets/images/fphone.png" alt="">{{$contact->phone}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="site_map">
                            <div class="site_map_title">
                                <h3>SITE MAP</h3>
                            </div>
                            <div class="site_map_menu">
                                <ul>
                                    <li><a href="">HOME</a></li>
                                    <li><a href="">PROJECTS</a></li>
                                    <li><a href="">SERVICES</a></li>
                                    <li><a href="">CLIENTS</a></li>
                                    <li><a href="">OUR CLIENTS</a></li>
                                    <li><a href="">CONTACTS</a></li>
                                    <li><a href="">OUR TEAM</a></li>
                                    <li><a href="">CARRIER</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer_follow">
                            <div class="footer_follow_title">
                                <h3>FOLLOW US</h3>
                            </div>
                            <div class="social_icons">
                                <div class="social_share">
                                    <a href="{{ $contact->youtube }}"><img src="/assets/images/youtube.png" alt=""></a>
                                </div>
                                <div class="social_share">
                                    <a href="{{ $contact->facebook }}"><img src="/assets/images/fb.png" alt=""></a>
                                </div>
                                <div class="social_share">
                                    <a href="{{ $contact->linkedin }}"><img src="/assets/images/in.png" alt=""></a>
                                </div>
                            </div>
                            <div class="all_right">
                                <span>All rights reserved © 2020 <a href="">CMC</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="up">
        <a href="#header_fluid"><img src="/assets/images/up.png" alt=""></a>
    </div>
</footer>
