<header id="header_fluid">
    <div class="space">
        <div class="smMenu">
            <div class="menuBar">
                <img src="/assets/images/menu.png" alt="">
            </div>
            <div class="smMenuSlide">
                <img src="/assets/images/x.png" alt="" class="closeMenu">
                <div class="smMenuList">
                    <ul>
                        @foreach(\App\Menu::orderBy('position','asc')->get() as $menu)

                        
                        <li><a href="/{{app()->getLocale()}}/{{$menu->slug}}">{{$menu->{'title_'.$locale} }}</a></li>
                       
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main_logo">
        <a href="/"><img src="/assets/images/logo.png" alt=""></a>
    </div>
    <div class="main_nav">

        <div class="main_menu">
            <ul>
                @foreach(\App\Menu::orderBy('position','asc')->get() as $menu)

                @if(count($menu->getChildren) == 0 && ($menu->getParent)== null)
                <li><a href="/{{app()->getLocale()}}/{{$menu->slug}}">{{$menu->{'title_'.$locale} }}</a></li>
                @elseif(count($menu->getChildren) > 0)
                <li class="haschild">
                    <a href="javascript:void(0)">{{$menu->{'title_'.$locale} }}</a>
                    <ul class="drop_menu">
                        @foreach($menu->getChildren as $child)
                        <li><a href="/{{app()->getLocale()}}/{{$child->slug}}">{{$child->{'title_'.$locale} }}</a></li>
                        @endforeach
                    </ul>
                </li>
                @endif
                @endforeach

            </ul>
        </div>

    </div>
    <div class="language_nav">
        <span @if($locale=='en') class="active" @endif><a href="/locale/en">ENG</a></span>
        <span @if($locale=='ka') class="active" @endif><a href="/locale/ka">GEO</a></span>
    </div>

</header>
