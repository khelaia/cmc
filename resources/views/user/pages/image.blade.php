@extends('user.layouts.app')

@section('content')
    <div class="gallery_container">


        @include('user.includes.header')


    </div>


    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="/{{$locale}}">Home</a>
                <a href="/{{$locale}}/gallery" class="title_arrows">Photo & Video Gallery</a>
            </div>
            <div class="page_fluid_text">
                <h1>Photo & Video Gallery</h1>
            </div>
        </div>
        <div class="container">
            <div class="gallery_title">
                <h2>VIDEOS <img src="/assets/images/projectsbg.png" alt=""></h2>
                <a href="">ALL VIDEOS <img src="/assets/images/right.png" alt=""> </a>
            </div>
        </div>


        <div class="container">
            <div class="gallery_title">
                <h2>PHOTOS <img src="/assets/images/projectsbg.png" alt=""></h2>
                <a href="">ALL PHOTOS <img src="/assets/images/right.png" alt=""> </a>
            </div>
        </div>
        <div class="main_gallery_slider">
            <div class="zoom-gallery">


                @foreach($images as $imagesPair)

                    <div class="projects_main_inside">
                        <?php $i = 0; ?>
                        @foreach($imagesPair as $imgs)
                            <div class="projects_inside_bottom">

                                @foreach($imgs as $image)

                                    @if($i==0 or $i==3)
                                        <div class="pib_left"> @else
                                                <div class="pib_right"> @endif
                                                    <a href="/uploads/{{ $image->image }}" data-source=""
                                                       title="{{$image->{'title_'.$locale} }}">
                                                        <img src="/uploads/{{ $image->image }}">
                                                        <div class="gallery_in_block video_in_block">

                                                            <div class="play_video">
                                                                <img src="/assets/images/camera.png" alt="">
                                                                <span>VIEW PHOTO</span>
                                                            </div>
                                                            <div class="gallery_cover">
                                                                <img src="/assets/images/galleryhover.png" alt="">
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php $i++; ?>
                                @endforeach
                            </div>
                        @endforeach

                    </div>
                @endforeach

            </div>

        </div>
    </div>

@endsection
