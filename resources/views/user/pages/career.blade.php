@extends('user.layouts.app')

@section('content')
    <div class="members_container">


        @include('user.includes.header')

        <div class="about_pages_blur">
            <img src="/assets/images/aboublur.png" alt="">
        </div>

        <div class="about_page_share">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>

    </div>

    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="/{{ $locale }}">@lang('career.home')</a>
                <a href="/{{$locale}}/career" class="title_arrows">@lang('career.career')</a>
            </div>
            <div class="page_fluid_text">
                <h1>@lang('career.career')</h1>
            </div>
        </div>
        <div class="container">
            <div class="accordion" id="accordionExample">
                @foreach($careers as $career)

                <div class="card">
                <a data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                {{ $career->{'title_'.$locale} }}
                            </button>
                        </h2>
                        <span>{{ $career->{'deadline_'.$locale} }}</span>
                        <img src="/assets/images/cariertop.png" alt="">
                    </div>

                </a>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                         data-parent="#accordionExample">
                        <div class="card-body">
                            <p>{{ $career->{'description_'.$locale} }}
                            </p>
                            <form method="POST" action="{{ route('vacancies.store') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="career_id" value="{{$career->id}}">
                                <div class="form-row">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="@lang('career.first_name'), @lang('career.last_name')" name="name" required>
                                    </div>
                                    <div class="col">
                                        <input type="email" class="form-control" placeholder="@lang('career.email')" name="email" required>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <input type="number" class="form-control" placeholder="@lang('career.phone')" name="phone" required>
                                    </div>
                                    <div class="col">
                                        <input type="file" class="form-control" placeholder="File Atach" name="file" accept=".pdf" required >
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">@lang('career.submit')</button>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>



        <div class="clients_slider">
            <div class="container">
                <div class="col-md-12">
                    <h3>@lang('career.our_clients')</h3>
                </div>
            </div>
            <div class="clients_slider_row">
                <div class="swiper-container4">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="client_column">
                                <a href=""><img src="/assets/images/bog.png" alt=""></a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client_column">
                                <a href=""><img src="/assets/images/bog.png" alt=""></a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client_column">
                                <a href=""><img src="/assets/images/bog.png" alt=""></a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client_column">
                                <a href=""><img src="/assets/images/bog.png" alt=""></a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client_column">
                                <a href=""><img src="/assets/images/bog.png" alt=""></a>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="client_column">
                                <a href=""><img src="/assets/images/bog.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
