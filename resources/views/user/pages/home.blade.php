@extends('user.layouts.app')

@section('content')
    <div id="main-page"></div>

    <div class="main_container">


        @include('user.includes.header')


        <div class="header_blur">
            <img src="/assets/images/secondblur.png" alt="">
        </div>
        <div class="services_blue">
            <img src="/assets/images/dots.png" alt="">
        </div>
        <div class="swiper-container2">
            <div class="swiper-wrapper">
                @foreach($sliders as $slider)
                <div class="swiper-slide">
                    <div class="top_slider"
                         style="background-image: radial-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .2)), url(/uploads/{{ $slider->image }})">
                        <div class="slider_intro">
                            <h1>{{ mb_substr($slider->title_ka,0,25) }}</h1>
                            <span>{{ $slider->address_ka }}</span>
                            <span>{{ $slider->author_ka }}</span>
                            @if(!empty($slider->url))
                            <a href="{{ $slider->url }}">@lang('home.view_project')</a>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper_arrows"></div>
            <!-- Add Arrows -->
            <div class="arrows">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>

    <div class="services_background">
        <div class="services_container">
            <div class="container">
                <div class="services_title">
                    <h2>@lang('home.services')</h2>
                    <img src="/assets/images/servicebg.png" alt="">
                </div>
            </div>
            <div class="services_column">
                <div class="container">
                    <div class="row">
                        @foreach($services as $service)
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/uploads/{{ $service->icon }}" alt="">
                                    <h2>{{ $service->title_ka }}</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>{{$service->description_ka}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="projects_container">
        <img src="/assets/images/projectbg.png" alt="">
        <div class="container">
            <div class="titles_container" >
                <div class="projects_title">
                    <h2>@lang('home.projects')</h2>
                    <img src="/assets/images/projectsbg.png" alt="">
                </div>
                <div id="options">
                    <div id="grid-filter" class="projects_filters">

                        <button id="all" class="button is-checked" data-filter="*">@lang('home.all')</button>
                        <button id="one" class="button" data-filter="ongoing">@lang('home.ongoing')</button>
                        <button id="two" class="button" data-filter="completed">@lang('home.completed')</button>

                    </div>
                </div>

                <div class="projects_full">
                    <span><a href="/{{$locale}}/projects">@lang('home.see_all_projects')</a></span>
                    <a href=""><img src="/assets/images/seeallbg.png" alt=""></a>
                </div>
            </div>
        </div>

        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
            <div class="projects_slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper" id="projects-area">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clients_background">
        <div class="clients_top">
            <img src="/assets/images/clientbg.png" alt="">
        </div>
        <div class="clients_container">

            <div class="container">
                <div class="clients_title">
                    <h2>@lang('home.clients')</h2>
                    <img src="/assets/images/clientsbg.png" alt="">
                </div>
            </div>

            <div class="clients_section">
                <div class="container">
                    <div class="row">
                    @foreach($clients as $client)
                        <div class="col-md-3">
                            <div class="clients_block">
                                <a href=""><img src="/uploads/{{ $client->image }}" alt=""></a>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="team_container">
        <img src="/assets/images/teamsbg.png" alt="">
        <div class="container">
            <div class="titles_container">
                <div class="projects_title">
                    <h2>@lang('home.our_team')</h2>
                    <img src="/assets/images/teambg.png" alt="">
                </div>
                <div class="projects_full">
                    <span><a href="">@lang('home.see_all_team')</a></span>
                    <a href=""><img src="/assets/images/seeallbg.png" alt=""></a>
                </div>
            </div>
        </div>

        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">

            <div class="team_slider">
                <div class="swiper-container3">
                    <div class="swiper-wrapper">
                        @foreach($teams as $teamsList)
                        <div class="swiper-slide">
                            <div class="team_column">
                                @foreach($teamsList as $team)
                                <div class="team_member" style="background-image: url(/uploads/{{$team->image}})">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>{{$team->{'name_'.$locale} }}</h4>
                                        <span>{{$team->{'job_'.$locale} }}</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href="{{$team->linkedin}}"><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>

                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>

        <div class="team_bg"></div>
        <div class="team_bottom">
            <img src="/assets/images/teambottom.png" alt="">
        </div>
    </div>
@endsection


@section('footer')
    <script>
        $(document).ready(function(){
            var data = []
            var countProjects = 8;
            $.getJSON('/api/projects?main=true&take='+countProjects,function (data) {
                var _html = '';
                var j=0;

                data = data.data;
                var stop = 0;
                while(stop <2) {
                    _html+='<div class="swiper-slide" ><div class="projects_main_inside">';
                    for (var i = 0; i < 2; i++) {

                        _html += '<div class="projects_inside_top">';
                        for (var k = 0; k < 2; k++) {
                            if (i % 2 == 1 && k == 0 || i % 2 == 0 && k == 1) {
                                _html += '<a href="/{{$locale}}/project/' + data[j].slug + '"><div class="pit_right">';
                            } else if (i % 2 == 0 && k == 0 || i % 2 == 1 && k == 1) {
                                _html += '<a href="/{{$locale}}/project/' + data[j].slug + '"><div class="pit_left">';
                            }
                            _html += '<div class="projects_in_block" style="background-image: url(/uploads/' + data[j].image + ')">';
                            _html += '<img src="/assets/images/projectblur.png" alt="">';
                            _html += '<div class="projects_info">';
                            _html += '<h4>' + data[j].title_{{$locale}}+ '</h4>';
                            _html += '<span><img src="/assets/images/location.png" alt="">' + data[j].status_{{$locale}}+ '</span>';
                            _html += '</div>';
                            _html += '</div>';
                            _html += '</div></a>';
                            console.log(j)
                            j++;
                        }
                        _html += '</div>';
                    }
                    _html+='</div></div>';
                    stop++;
                }
                document.getElementById('projects-area').innerHTML = _html;

            });

            $('#grid-filter button').on('click',function () {
                var dataFilter = $(this).attr('data-filter');
                var dataFilterQuery = dataFilter!=='*'?'&status='+dataFilter:'';
                $.getJSON('/api/projects?homepage=true&pretty=true&main=true&take='+countProjects+dataFilterQuery,function (data) {
                    var _html = '';
                    var j=0;

                    data = data;
                    console.log('length is ',data)

                    for(var k=0; k<data.length;k++) {
                        _html+='<div class="swiper-slide" ><div class="projects_main_inside">';
                        var t = 0;
                        for (var i = 0; i < data[k].length; i++) {

                            _html += '<div class="projects_inside_top">';
                            console.log(data[k][i]);
                            for (var j = 0; j < data[k][i].length; j++) {
                                if (t==0 || t==3) {
                                    _html += '<a href="/{{$locale}}/project/' + data[k][i][j].slug + '"><div class="pit_right">';
                                } else{
                                    _html += '<a href="/{{$locale}}/project/' + data[k][i][j].slug + '"><div class="pit_left">';
                                }
                                _html += '<div class="projects_in_block" style="background-image: url(/uploads/' + data[k][i][j].cover + ')">';
                                _html += '<img src="/assets/images/projectblur.png" alt="">';
                                _html += '<div class="projects_info">';
                                _html += '<h4>' + data[k][i][j].title_{{$locale}}+ '</h4>';
                                _html += '<span><img src="/assets/images/location.png" alt="">' + data[k][i][j].status_{{$locale}}+ '</span>';
                                _html += '</div>';
                                _html += '</div>';
                                _html += '</div></a>';

                                t++;
                            }
                            _html += '</div>'
                        }
                        _html+='</div></div>';
                    }
                    document.getElementById('projects-area').innerHTML = _html;

                });
            })


        });
    </script>
@endsection
