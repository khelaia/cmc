@extends('user.layouts.app')
@section('description',$seo->{'description_'.$locale})
@section('keywords',$seo->{'keywords_'.$locale})
@section('content')
    <div class="members_container">


        @include('user.includes.header')

        <div class="about_pages_blur">
            <img src="/assets/images/aboublur.png" alt="">
        </div>

        <div class="about_page_share">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>

    </div>



    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots_top">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="">Home</a>
                <a href="" class="title_arrows">Services</a>
            </div>
            <div class="page_fluid_text">
                <h1>Services</h1>
            </div>
        </div>
        <div class="services_column">
            <div class="container">
                <div class="row">
                    @foreach($services as $service)
                    <div class="col-md-3">
                        <div class="service_in_container">
                            <div class="services_in_title">
                                <img src="/assets/images/service1.png" alt="">
                                <h2>{{ $service->title_ka }}</h2>
                            </div>
                            <div class="service_in_info">
                                <p>{{$service->description_ka}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
