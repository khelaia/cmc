@extends('user.layouts.app')

@section('content')

<div class="members_container">

    @include('user.includes.header')

    <div class="about_pages_blur">
        <img src="/assets/images/aboublur.png" alt="">
    </div>

    <div class="about_page_share">
        <span>SHARE</span>
        <div class="social_share">
            <a href=""><img src="/assets/images/youtube.png" alt=""></a>
        </div>
        <div class="social_share">
            <a href=""><img src="/assets/images/fb.png" alt=""></a>
        </div>
        <div class="social_share">
            <a href=""><img src="/assets/images/in.png" alt=""></a>
        </div>
    </div>

</div>


<div class="page_fluid">
    <div class="inside_bg">
        <img src="/assets/images/projectbg.png" alt="">
    </div>
    <div class="inside_dots_top">
        <img src="/assets/images/insidedots.png" alt="">
    </div>
    <div class="container">
        <div class="title_tags">
            <a href="">Home</a>
            <a href="" class="title_arrows">Contacts</a>
        </div>
        <div class="page_fluid_text">
            <h1>Contact</h1>
        </div>
    </div>
    <div class="contact_container">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="contact_space">
                        <img src="/assets/images/contact_location.png" alt="">
                        <span>{{ $contact->{'address_'.$locale} }}</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact_space">
                        <img src="/assets/images/contact_msg.png" alt="">
                        <span>{{ $contact->email }}</span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact_space">
                        <img src="/assets/images/contact_phone.png" alt="">
                        <span>{{ $contact->phone }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h2>უკუკავშირი</h2>
        <div class="contact-body">
            <form>
                <div class="form-row">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="First Name">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="E-mail">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="თემა">
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <textarea id="contactform" class="md-textarea form-control" rows="3" placeholder="შეტყობინება"></textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

</div>


@endsection
