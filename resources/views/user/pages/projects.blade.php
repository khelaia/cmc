@extends('user.layouts.app')



@section('content')
    <div class="gallery_container">


        @include('user.includes.header')


    </div>

    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots_top">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="/{{$locale}}">@lang('projects.home')</a>
                <a href="/{{$locale}}/projects" class="title_arrows">@lang('projects.projects')</a>
            </div>
            <div class="page_fluid_text fluid_flexible">
                <h1>@lang('projects.projects')</h1>
                <div id="grid-filter" class="projects_filters">

                    <button id="all" class="button is-checked" data-filter="*">@lang('projects.all')</button>
                    <button id="one" class="button" data-filter="ongoing">@lang('projects.ongoing')</button>
                    <button id="two" class="button" data-filter="completed">@lang('projects.completed')</button>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="projects_main_inside" id="projects-area">

            </div>
            <div class="projects_load_more">
                <button id="load-more" type="submit" class="btn btn-primary">@lang('projects.load_more') <img src="/assets/images/pageright.png"
                                                                             alt=""></button> </div>
        </div>

    </div>
@endsection


@section('footer')
    <script>
        $(document).ready(function(){
            var data = []
            var countProjects = 6;
            $.getJSON('/api/projects?take='+countProjects,function (data) {
                var _html = '';
                var j=0;

                data = data.data;

                for (var i=0;i<(data.length/2);i++ ){

                    _html+='<div class="projects_inside_top">';
                    for (var k=0;k<2;k++){
                        if (i%2==1 && k==0 || i%2==0 && k==1){
                            _html+='<a href="/{{$locale}}/project/'+data[j].slug+'"><div class="pit_right">';
                        }else if(i%2==0 && k==0 || i%2==1 && k==1){
                            _html+='<a href="/{{$locale}}/project/'+data[j].slug+'"><div class="pit_left">';
                        }
                                _html+='<div class="projects_in_block" style="background-image: url(/uploads/'+data[j].image+')">';
                                    _html+='<img src="/assets/images/projectblur.png" alt="">';
                                    _html+='<div class="projects_info">';
                                        _html+='<h4>'+data[j].title_{{$locale}}+'</h4>';
                                        _html+='<span><img src="/assets/images/location.png" alt="">'+data[j].status_{{$locale}}+'</span>';
                                    _html+='</div>';
                                _html+='</div>';
                            _html+='</div></a>';
                        j++;
                    }
                    _html+='</div>'
                }
                document.getElementById('projects-area').innerHTML = _html;

            });

            $('#grid-filter button').on('click',function () {
                var dataFilter = $(this).attr('data-filter');
                var dataFilterQuery = dataFilter!=='*'?'&status='+dataFilter:'';
                $.getJSON('/api/projects?take='+countProjects+dataFilterQuery,function (data) {
                    var _html = '';
                    var j=0;

                    data = data.data;

                    for (var i=0;i<(data.length/2);i++ ){

                        _html+='<div class="projects_inside_top">';
                        for (var k=0;k<2;k++){
                            if (i%2==1 && k==0 || i%2==0 && k==1){
                                _html+='<a href="/{{$locale}}/project/'+data[j].slug+'"><div class="pit_right">';
                            }else if(i%2==0 && k==0 || i%2==1 && k==1){
                                _html+='<a href="/{{$locale}}/project/'+data[j].slug+'"><div class="pit_left">';
                            }
                            _html+='<div class="projects_in_block" style="background-image: url(/uploads/'+data[j].image+')">';
                            _html+='<img src="/assets/images/projectblur.png" alt="">';
                            _html+='<div class="projects_info">';
                            _html+='<h4>'+data[j].title_{{$locale}}+'</h4>';
                            _html+='<span><img src="/assets/images/location.png" alt="">'+data[j].status_{{$locale}}+'</span>';
                            _html+='</div>';
                            _html+='</div>';
                            _html+='</a></div>';
                            j++;
                        }
                        _html+='</div>'
                    }
                    document.getElementById('projects-area').innerHTML = _html;

                });
            })

            $('#load-more').on('click',function () {
                countProjects+=2
                if (countProjects >= {{ $total }}){
                    $('#load-more').remove();
                }
                var dataFilterQuery = typeof dataFilter !== 'undefined'?'&status='+dataFilter:'';
                $.getJSON('/api/projects?take='+countProjects+dataFilterQuery,function (data) {
                    var _html = '';
                    var j=0;

                    data = data.data;

                    for (var i=0;i<(data.length/2);i++ ){

                        _html+='<div class="projects_inside_top">';
                        for (var k=0;k<2;k++){
                            if (i%2==1 && k==0 || i%2==0 && k==1){
                                _html+='<a href="/{{$locale}}/project/'+data[j].slug+'"><div class="pit_right">';
                            }else if(i%2==0 && k==0 || i%2==1 && k==1){
                                _html+='<a href="/{{$locale}}/project/'+data[j].slug+'"><div class="pit_left">';
                            }
                            _html+='<div class="projects_in_block" style="background-image: url(/uploads/'+data[j].image+')">';
                            _html+='<img src="/assets/images/projectblur.png" alt="">';
                            _html+='<div class="projects_info">';
                            _html+='<h4>'+data[j].title_{{$locale}}+'</h4>';
                            _html+='<span><img src="/assets/images/location.png" alt="">'+data[j].status_{{$locale}}+'</span>';
                            _html+='</div>';
                            _html+='</div>';
                            _html+='</a></div>';
                            j++;
                        }
                        _html+='</div>'
                    }
                    document.getElementById('projects-area').innerHTML = _html;

                });
            })
        });
    </script>
@endsection
