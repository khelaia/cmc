@extends('user.layouts.app')
@section('content')
    <div class="members_container">


        @include('user.includes.header')

        <div class="about_pages_blur">
            <img src="/assets/images/aboublur.png" alt="">
        </div>

        <div class="about_page_share">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>

    </div>

    </div>

    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots_top">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="inside_dots">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="/{{ $locale }}">@lang('team.home')</a>
                <a href="/{{$locale}}/our-team" class="title_arrows">@lang('team.our_team')</a>
            </div>
            <div class="page_fluid_text">
                <h1>@lang('team.our_team')</h1>
                <p>Experience of the founders of the company starts back in the year of 2000. Considering personal
                    knowledge construction industry and detailed observation on local market it was clear there was a
                    need for service to be introduced into the Georgian construction market that might ensure a clients`
                    interest to come first and meet their demands to resolve financial optimization problems and
                    guarantee the quality within time.</p>
            </div>
        </div>
        @foreach($categories as $category)
        <div class="members_column">
            <div class="container">
                <div class="col-md-12">
                    <h2>{{$category->name_ka}}</h2>
                </div>
                <div class="row">
                    @foreach($category->staff as $staff)
                        @if($staff->status == 1)
                        <div class="col-xl-3 col-md-4 col-sm-6">
                            <div class="member_column">
                                <img src="/uploads/{{ $staff->image }}" alt="">
                                <h3>{{$staff->name_ka}}</h3>
                                <span>{{ $staff->job_ka }}</span>
                                <div class="member_cover">
                                    <img src="/assets/images/memberhover.png" alt="">
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach

    </div>
@endsection
