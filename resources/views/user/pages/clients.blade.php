@extends('user.layouts.app')

@section('content')
    <div class="members_container">


        @include('user.includes.header')

        <div class="about_pages_blur">
            <img src="/assets/images/aboublur.png" alt="">
        </div>

        <div class="about_page_share">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>

    </div>



    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots_top">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="">Home</a>
                <a href="" class="title_arrows">Clients</a>
            </div>
            <div class="page_fluid_text">
                <h1>Clients</h1>
            </div>
        </div>
        <div class="clients_column">
            <div class="container">
                <div class="row">
                    @foreach($clients as $client)
                    <div class="col-xl-3 col-md-4 col-sm-6">
                        <div class="client_logo">
                            <img src="/uploads/{{$client->image}}" alt="">
                            <a href="{{ $client->link }}">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
