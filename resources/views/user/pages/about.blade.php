@extends('user.layouts.app')

@section('content')
    <div class="members_container">


        @include('user.includes.header')

        <div class="about_pages_blur">
            <img src="/assets/images/aboublur.png" alt="">
        </div>

        <div class="about_page_share">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>

    </div>

    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="/{{ $locale }}">@lang('about.home')</a>
                <a href="/{{ $locale }}/whoweare" class="title_arrows">@lang('about.whoweare')</a>
            </div>
            <div class="page_fluid_text">
                <h1>@lang('about.whoweare')</h1>
            </div>
        </div>
        <div class="who_we_are">
            <div class="container">
                {!!  $about->{'description_'.$locale} !!}
            </div>
        </div>
        <div class="projects_in_slider">
            <div class="container">
                <div class="col-md-12">
                    <div class="projects_in_title">
                        <h3>Hightlighted projects</h3>
                        <div class="projects_full">
                            <span><a href="/{{$locale}}">@lang('about.all_projects')</a></span>
                            <a href="/{{ $locale }}/whoweare"><img src="/assets/images/seeallbg.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="projects_slider_row">
                <div class="swiper-container5">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="projects_in_column">
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="projects_in_column">
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="projects_in_column">
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider_line"></div>
            </div>
        </div>
    </div>

@endsection
