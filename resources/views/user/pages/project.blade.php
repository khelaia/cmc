@extends('user.layouts.app')
@section('description',$project->{'description_'.$locale})
@section('keywords',$project->{'keywords_'.$locale})
@section('content')
    <div class="project_container">


        @include('user.includes.header')

        <div class="header_blur">
            <img src="/assets/images/secondblur.png" alt="">
        </div>
        <div class="services_blue">
            <img src="/assets/images/dots.png" alt="">
        </div>
        <div class="swiper-container6">
            <div class="swiper-wrapper">
                @foreach(json_decode($project->gallery) as $gallery)
                <div class="swiper-slide">
                    <div class="top_project_slider"
                         style="background-image: radial-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .2)), url(/uploads/{{ $gallery }})">
                    </div>
                </div>
                @endforeach


            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper_arrows"></div>
            <!-- Add Arrows -->
            <div class="arrows">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
        <div class="about_page_share_left">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>
    </div>

    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="">Home</a>
                <a href="/{{$locale}}/projects" class="title_arrows">Projects</a>
                <a href="/{{$locale}}/project/{{$project->id}}" class="title_arrows">{{ $project->{'title_'.$locale} }}</a>
            </div>
            <div class="page_fluid_text">
                <h1>{{ $project->{'title_'.$locale} }}</h1>
            </div>
        </div>

        <div class="project_small_info">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="small_info_main">
                            <span><b>Project:</b>{{ $project->{'title_'.$locale} }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small_info_main">
                            <span><b>Budget:</b>{{ $project->{'budget_'.$locale} }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small_info_main">
                            <span><b>Cmc scope:</b>{{ $project->{'scope_'.$locale} }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small_info_main">
                            <span><b>Location:</b>{{ $project->{'location_'.$locale} }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small_info_main">
                            <span><b>Project area: </b>{{ $project->{'area_'.$locale} }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small_info_main">
                            <span><b>Status:</b>{{ $project->{'status_'.$locale} }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="small_info_main">
                            <span><b>Client:</b>{{ $project->{'customer_'.$locale} }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="project_full_info">
            <div class="container">
                {!! $project->{'description_'.$locale} !!}
            </div>
        </div>

    </div>
@endsection
