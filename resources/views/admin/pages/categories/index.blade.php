@extends('admin.layouts.app')

@section('content')
    <div class="app-content content">
        <div class="content-header row">
        </div>
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Categories</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin">Admin</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/admin/categories">Categories</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 mb-2 text-right">
                    <a class="btn btn-primary waves-effect waves-light" href="/admin/categories/create">Add new</a>
                </div>
            </div>
            <div class="content-body">
                <!-- Default styling start -->
                <div class="row" id="default">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Default styling</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard ">
                                    <p class="card-text"></p>
                                </div>
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table mb-0" id="formTable" data-sortable>
                                        <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Name</th>
                                            <th>Name EN</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="sortThis">
                                        @foreach($categories as $category)
                                            <tr data-index="{{ $category->id }}" data-position="{{ $category->position }}">
                                                <td>{{ $category->id }}</td>
                                                <td>{{ $category->name_ka }}</td>
                                                <td>{{ $category->name_en }}</td>
                                                <td class="text-right">
                                                    <form action="{{ route('admin::categories.destroy',$category->id) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <div class="btn-group arrow-right" role="group" aria-label="Basic example">
                                                            <a type="button" class="btn btn-primary" href="{{ route('admin::categories.edit',$category->id) }}">Edit</a>
                                                            <button type="submit" class="btn btn-danger">Remove</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Default styling end -->

            </div>
        </div>
    </div>
@endsection

@section('position-update')
    <script type="text/javascript">
        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
                $(this).removeClass('updated');
            });

            $.ajax({
                url: '{{ route('admin::position_update_categories') }}',
                method: 'PUT',
                dataType: 'text',
                data: {
                    updated: 1,
                    positions: positions,
                    _token: '{{csrf_token()}}'
                }, success: function (response) {
                    console.log(response);
                },error: function (data, textStatus, errorThrown) {
                    console.log(data);

                },
            });
        }
    </script>
@endsection
