@extends('admin.layouts.app')

@section('content')
    <div class="app-content content">
        <div class="content-header row"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="horz-layout-basic">Staff Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collpase show">
                                    <div class="card-body">
                                        <div class="card-text">
                                            <p></p>
                                        </div>
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form class="form form-horizontal" method="POST" action="{{ route('admin::staff.update',$staff->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-clipboard"></i>Edit Staff</h4>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Full Name KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Full Name KA" name="name_ka" value="{{ $staff->name_ka }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Full Name EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Full Name EN" name="name_en" value="{{ $staff->name_en }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Job KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Job KA" name="job_ka" value="{{ $staff->job_ka }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Job EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Job EN" name="job_en" value="{{ $staff->job_en }}" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control">Category</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <select class="hide-search form-control" id="hide_search" name="category_id" required>
                                                            @foreach($categories as $category)
                                                                <option value="{{ $category->id }}" @if($category->id==$staff->category_id) selected @endif>{{ $category->name_ka }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control">Profile Image</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <label id="projectinput8" class="file center-block">
                                                            <input name="image" type="file" id="file">
                                                            <span class="file-custom"></span>
                                                        </label>
                                                        <span class="media-left avatar-online">
                                                            <img class="media-object" src="/uploads/{{ $staff->image }}"  style="height: 64px; object-fit: cover">
                                                            <i></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Linkedin</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Linkedin" name="linkedin" value="{{ $staff->linkedin }}" required>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-9 mx-auto">
                                                        <input name="status" type="checkbox" id="switcheryColor4" class="switchery" data-color="success" {{ $staff->status?'checked':'' }} />
                                                        <label for="switcheryColor4" class="card-title ml-1">Published</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-9 mx-auto">
                                                        <input name="main" type="checkbox" id="switcheryColor4" class="switchery" data-color="success" {{ $staff->main?'checked':'' }} />
                                                        <label for="switcheryColor4" class="card-title ml-1">Show Home Page</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions">

                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
@endsection

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/material-vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/simple-line-icons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-switch.css') }}">
@endsection
@section('footer')
    <script src="{{ asset('tinymce/tinymce.js') }}"></script>
    <script>
        tinymce.init({
            selector:'textarea#tiny_en',
            height:500,
            plugins: ' table ',
        });
        tinymce.init({
            selector:'textarea#tiny_ka',
            height:500,
            plugins: ' table ',
        });
    </script>


    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/ui/prism.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/material-app.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/extensions/dropzone.js') }}"></script>
    <!-- END: Page JS-->
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/switchery.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js') }}"></script>

    <script src="{{ asset('app-assets/js/scripts/pages/material-app.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}"></script>


@endsection
