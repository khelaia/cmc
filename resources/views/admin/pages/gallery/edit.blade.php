@extends('admin.layouts.app')

@section('content')
    <div class="app-content content">
        <div class="content-header row"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="horz-layout-basic">Gallery Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collpase show">
                                    <div class="card-body">
                                        <div class="card-text">
                                            <p></p>
                                        </div>
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form class="form form-horizontal" method="POST" action="{{ route('admin::galleries.update',$gallery->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-clipboard"></i>Gallery</h4>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Title KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Title KA" name="title_ka" value="{{ $gallery->title_ka }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Title EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Title EN" name="title_en" value="{{ $gallery->title_en }}">
                                                    </div>
                                                </div>

                                                @if($gallery->category == "image")
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control">File</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <label id="projectinput8" class="file center-block">
                                                            <input name="file" type="file" id="file">
                                                            <span class="file-custom"></span>
                                                        </label>
                                                        <span class="media-left avatar-online">
                                                            <img class="media-object" src="/uploads/{{ $gallery->image }}" style="height: 64px; object-fit: cover">
                                                            <i></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                @elseif($gallery->category == "video")
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Youtube Link</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="url" id="projectinput5" class="form-control" placeholder="Youtube Link" name="video" value="https://www.youtube.com/watch?v={{ $gallery->video }}">
                                                        </div>
                                                    </div>
                                                @endif

                                            </div>

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
@endsection

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/material-vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/simple-line-icons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-switch.css') }}">
@endsection
@section('footer')


    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/ui/prism.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/material-app.js') }}"></script>
    <!-- END: Page JS-->
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/switchery.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js') }}"></script>

    <script src="{{ asset('app-assets/js/scripts/pages/material-app.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}"></script>


@endsection
