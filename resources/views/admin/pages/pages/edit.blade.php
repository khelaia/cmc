@extends('admin.layouts.app')

@section('content')
    <div class="app-content content">
        <div class="content-header row"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="horz-layout-basic">Page Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collpase show">
                                    <div class="card-body">
                                        <div class="card-text">
                                            <p></p>
                                        </div>
                                        @if ($errors->any())
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif
                                        <form class="form form-horizontal" method="POST" action="{{ route('admin::pages.update',$page->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-clipboard"></i>Services</h4>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Title KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Title KA" name="title_ka" value="{{ $page->title_ka }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Title EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Title EN" name="title_en" value="{{ $page->title_en }}">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Keywords KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset class="form-group">
                                                            <textarea name="keywords_ka" class="form-control" id="placeTextarea" rows="3" placeholder="Keywords KA">{{ $page->keywords_ka }}</textarea>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Keywords KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset class="form-group">
                                                            <textarea name="keywords_en" class="form-control" id="placeTextarea" rows="3" placeholder="Keywords EN">{{ $page->keywords_en }}</textarea>
                                                        </fieldset>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Description KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset class="form-group">
                                                            <textarea name="description_ka" class="form-control" id="placeTextarea" rows="3" placeholder="Description KA">{{ $page->description_ka }}</textarea>
                                                        </fieldset>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Description EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset class="form-group">
                                                            <textarea name="description_en" class="form-control" id="placeTextarea" rows="3" placeholder="Description EN">{{ $page->description_en }}</textarea>
                                                        </fieldset>
                                                    </div>
                                                </div>



                                                <div class="form-group row">
                                                    <div class="col-md-9 mx-auto">
                                                        <input name="status" type="checkbox" id="switcheryColor4" class="switchery" data-color="success" {{ $page->status?'checked':'' }}  />
                                                        <label for="switcheryColor4" class="card-title ml-1">Published</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
@endsection

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/material-vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/toggle/switchery.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/switch.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/fonts/simple-line-icons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/colors/palette-switch.css') }}">
@endsection
@section('footer')


    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/extensions/dropzone.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/ui/prism.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ asset('app-assets/js/scripts/pages/material-app.js') }}"></script>
    <!-- END: Page JS-->
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/switchery.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js') }}"></script>

    <script src="{{ asset('app-assets/js/scripts/pages/material-app.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/forms/switch.js') }}"></script>


@endsection
