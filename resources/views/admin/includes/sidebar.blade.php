<div class="main-menu material-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="user-profile">
        <div class="user-info text-center pb-2"><img class="user-img img-fluid rounded-circle w-25 mt-2" src="{{ asset('app-assets/images/portrait/medium/avatar-m-1.png') }}" alt="" />
            <div class="name-wrapper d-block dropdown mt-1"><a class="white dropdown-toggle ml-2" id="user-account" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-name">Charlie Adams</span></a>
                <div class="text-light">Administrator</div>
                <div class="dropdown-menu arrow"><a class="dropdown-item"><i class="material-icons align-middle mr-1">power_settings_new</i><span class="align-middle">Log Out</span></a></div>
            </div>
        </div>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="active"><a href="/admin/dashboard"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="">Dashboard</span></a></li>
            <li class=" navigation-header"><span data-i18n="nav.category.ecommerce">Manage</span><i class="material-icons nav-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="Ecommerce">more_horiz</i></li>
            <li class=" nav-item"><a href="/admin/projects"><i class="material-icons">add_shopping_cart</i><span class="menu-title" data-i18n="">Projects</span></a></li>
            <li class=" nav-item"><a href="/admin/sliders"><i class="material-icons">add_shopping_cart</i><span class="menu-title" data-i18n="">Sliders</span></a></li>
            <li class="nav-item has-sub"><a href="#" class="waves-effect waves-dark"><i class="material-icons">tv</i><span class="menu-title" data-i18n="nav.templates.main">Staff</span></a>
                <ul class="menu-content" style="">
                    <li class=" nav-item"><a href="/admin/staff"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Staff</span></a>
                    </li>
                    <li class=" nav-item"><a href="/admin/categories"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Categories</span></a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="/admin/services"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Services</span></a></li>
            <li class=" nav-item"><a href="/admin/clients"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Clients</span></a></li>
            <li class=" nav-item"><a href="/admin/about"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">About</span></a></li>
            <li class=" nav-item"><a href="/admin/galleries"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Gallery</span></a></li>
            <li class=" nav-item"><a href="/admin/careers"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Career</span></a></li>
            <li class=" nav-item"><a href="/admin/vacancies"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Vacancies</span></a></li>
            <li class=" nav-item"><a href="/admin/pages"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Pages</span></a></li>
            <li class=" nav-item"><a href="/admin/menus"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Menus</span></a></li>
            <li class=" nav-item"><a href="/admin/contact"><i class="material-icons">format_list_numbered</i><span class="menu-title" data-i18n="">Contact</span></a></li>
        </ul>
    </div>
</div>
