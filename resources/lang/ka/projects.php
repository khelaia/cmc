<?php

return [
    'home'=>'მთავარი',
    'projects'=>'პროექტები',
    'all'=>'ყველა',
    'ongoing'=>'მიმდინარე',
    'completed'=>'დასრულებული',
    'load_more'=>'მეტის ჩატვირთვა',
    'all_projects'=>'ყველა პროექტი',
];
