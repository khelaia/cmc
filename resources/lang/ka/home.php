<?php

return [
    'view_project'=>'ნახე პროექტი',
    'services'=>'სერვისები',
    'all'=>'ყველა',
    'projects'=>'პროექტები',
    'ongoing'=>'მიმდინარე',
    'completed'=>'დასრულებული',
    'see_all_projects'=>'ნახე ყველა პროექტი',
    'clients'=>'კლიენტები',
    'our_team'=>'ჩვენი გუნდი',
    'see_all_team'=>'ნახე ყველა',
];
