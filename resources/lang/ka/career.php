<?php

return [
    'home'=>'მთავარი',
    'our_team'=>'ჩვენი გუნდი',
    'career'=>'კარიერა',
    'our_clients'=>'ჩვენი კლიენტები',
    'first_name'=>'სახელი',
    'last_name'=>'გვარი',
    'email'=>'ელ-ფოსტა',
    'phone'=>'ტელეფონი',
    'submit'=>'გაგზავნა'
];
