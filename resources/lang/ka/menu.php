<?php

return [
    'whoweare'=>'ვინ ვართ ჩვენ',
    'our_team'=>'ჩვენი გუნდი',
    'our_clients'=>'კლიენტები',
    'career'=>'კარიერა',
    'whatwedo'=>'რას ვაკეთებთ',
    'projects'=>'პროექტები',
    'contact'=>'კონტაქტი',
];
