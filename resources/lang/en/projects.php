<?php

return [
    'home'=>'Home',
    'projects'=>'Projects',
    'all'=>'All',
    'ongoing'=>'Ongoing',
    'completed'=>'Completed',
    'load_more'=>'Load More',
    'all_projects'=>'All Projects',
];
