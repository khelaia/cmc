<?php

return [
    'view_project'=>'View Project',
    'services'=>'Services',
    'all'=>'All',
    'projects'=>'Projects',
    'ongoing'=>'Ongoing',
    'completed'=>'Completed',
    'see_all_projects'=>'See All Projects',
    'clients'=>'Clients',
    'our_team'=>'Our Team',
    'see_all_team'=>'See all team',

];
