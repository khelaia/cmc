<?php

return [
    'home'=>'Home',
    'our_team'=>'Our Team',
    'career'=>'Career',
    'our_clients'=>'Our Clients',
    'first_name'=>'First Name',
    'last_name'=>'Last Name',
    'email'=>'E-mail',
    'phone'=>'Phone Number',
    'submit'=>'submit'
];
