<?php

return [
    'whoweare'=>'Who We Are',
    'our_team'=>'Our Team',
    'our_clients'=>'Clients',
    'career'=>'Career',
    'whatwedo'=>'What We Do',
    'projects'=>'Projects',
    'contact'=>'Contact',
];
