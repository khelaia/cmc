<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    protected $fillable = ['title_ka','title_en','deadline','description_ka','description_en','status'];
}
