<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function getChildren() {
        return $this->hasMany(Menu::class,'parent_id');
    }
    public function getParent() {
        return $this->belongsTo(Menu::class,'parent_id');
    }
}
