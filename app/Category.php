<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function staff(){
        return $this->hasMany(Staff::class)->orderBy('position','asc');
    }
}
