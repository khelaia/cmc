<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['address_ka','address_en','email','phone','facebook','youtube','linkedin'];
}
