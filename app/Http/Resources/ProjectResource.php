<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'title_ka'=>$this->title_ka,
            'title_en'=>$this->title_en,
            'image'=>$this->cover,
            'status_ka'=>$this->status_ka,
            'status_en'=>$this->status_en,
            'slug'=>$this->slug,
        ];
    }
}
