<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $clients = Client::orderBy('position','asc')->get();
        return view('admin.pages.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.pages.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'link'=>'required',
            'image'=>'required|mimes:png,ico'
        ]);

        $client = new Client();
        $client->link = $request->link;
        $client->status = $request->status == "on"?1:0;

        //image upload
        $fileName = time().rand().'.'.$request->image->extension();
        $request->image->move(public_path('uploads'), $fileName);

        $client->image = $fileName;

        $client->save();

        return redirect()->route('admin::clients.index')
            ->with('success','Client created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return Factory|View
     */
    public function edit(Client $client)
    {
        return view('admin.pages.clients.edit',compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return RedirectResponse
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'link'=>'required',
            'image'=>'mimes:png,ico'
        ]);

        $client->link = $request->link;
        $client->status = $request->status == "on"?1:0;


        //image upload
        if ($request->file('image')) {
            $fileName = time() . rand() . '.' . $request->image->extension();
            $request->image->move(public_path('uploads'), $fileName);

            if(file_exists(public_path('uploads/'.$client->image))){
                unlink(public_path('uploads/'.$client->image));
            }

            $client->image = $fileName;
        }

        $client->save();

        return redirect()->route('admin::clients.index')
            ->with('success','Client updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Client $client
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Client $client)
    {
        if(file_exists(public_path('uploads/'.$client->image))){
            unlink(public_path('uploads/'.$client->image));
        }

        $client->delete();

        return redirect()->route('admin::clients.index')
            ->with('success','Client Deleted Successfully.');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $clients = Client::find($index);

            $clients->position = $newPosition;

            $clients->save();
        }

        return response()->json('success');
    }
}
