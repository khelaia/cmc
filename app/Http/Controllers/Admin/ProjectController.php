<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $projects = Project::orderBy('position','asc')->get();
        return view('admin.pages.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.pages.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_ka' => 'required',
            'title_en' => 'required',
            'cover' => 'required|mimes:jpg,jpeg,png,gif|max:2048',
            'description_ka' => 'required',
            'description_en' => 'required',
            'gallery.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $data = $request->all();
        $data['status'] = $request->status == "on"?1:0;
        $data['status_ka'] = $request->status_en=='ongoing'?'მიმდინარე':'დასრულებული';

        //image upload
        $fileName = time().rand().'.'.$request->cover->extension();
        $request->cover->move(public_path('uploads'), $fileName);

        $data['cover'] = $fileName;
        $slug = str_slug($request->title_en);
        while (Project::where('slug','=',$slug)->exists()){
            $slug = $slug.'-'.rand(10,100);
        }
        $data['slug'] = $slug;

        $data['keywords_ka'] = json_encode($request->keywords_ka);
        $data['keywords_en'] = json_encode($request->keywords_en);


        //gallery images upload
        $gallery = [];
        if($request->file('gallery'))
        {

            foreach($request->file('gallery') as $image)
            {
                $name=time().rand().'.'.$image->extension();
                $image->move(public_path('uploads'), $name);
                $gallery[] = $name;
            }
        }
        unset($data['repeater-list']);

        $data['gallery'] = json_encode($gallery);

        Project::create($data);

        return redirect()->route('admin::projects.index')
            ->with('success','Project created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Project $project
     * @return Response
     */
    public function show(Project $project)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Project $project
     * @return Factory|View
     */
    public function edit(Project $project)
    {
        return view('admin.pages.projects.edit',compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Project $project
     * @return RedirectResponse
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'title_ka' => 'required',
            'title_en' => 'required',
            'cover' => 'image|mimes:jpg,jpeg,png,gif,svg|max:2048',
            'description_ka' => 'required',
            'description_en' => 'required',
            'gallery.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $data = $request->all();
        $data['status'] = $request->status == "on"?1:0;

        $data['status_ka'] = $request->status_en=='ongoing'?'მიმდინარე':'დასრულებული';
        $data['main'] = $request->main=='on'?1:0;

        //image upload
        if ($request->file('cover')){
            $fileName = time().rand().'.'.$request->cover->extension();
            $request->cover->move(public_path('uploads'), $fileName);

            if(file_exists(public_path('uploads/'.$project->cover))){
                unlink(public_path('uploads/'.$project->cover));
            }

            $data['cover'] = $fileName;
        }

        //gallery images upload
        $gallery = [];
        if($request->file('gallery'))
        {
            foreach($request->file('gallery') as $image)
            {
                $name=time().rand().'.'.$image->extension();
                $image->move(public_path('uploads'), $name);
                $gallery[] = $name;
            }
        }
        unset($data['repeater-list']);

        $data['keywords_ka'] = implode("|",$request->keywords_ka);
        $data['keywords_en'] = implode("|",$request->keywords_en);
        $data['gallery'] = json_encode(array_merge(json_decode($project->gallery,true) , $gallery));

        $project->update($data);

        return redirect()->route('admin::projects.index')
            ->with('success','Project Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Project $project)
    {
        if(file_exists(public_path('uploads/'.$project->cover))){
            unlink(public_path('uploads/'.$project->cover));
        }
        foreach (json_decode($project->gallery) as $image){
            if(file_exists(public_path('uploads/'.$image))){
                unlink(public_path('uploads/'.$image));
            }
        }
        $project->delete();

        return redirect()->route('admin::projects.index')
            ->with('success','Project Deleted successfully.');
    }


    public function updateGallery(Request $request){
        $request->validate([
            'id'=>'required|numeric'
        ]);
        $project = Project::findOrFail($request->id);
        $gallery = json_decode($project->gallery,true);

        $req_gallery = $request->gallery?$request->gallery:[];
        if (count($request->gallery) < count($gallery)){

            $removeList = array_diff($gallery,$req_gallery);

            foreach ($removeList as $remove){
                if(file_exists(public_path('uploads/'.$remove))){
                    unlink(public_path('uploads/'.$remove));
                }
            }
        }
        $project->gallery = json_encode($req_gallery);
        $project->save();

        return response()->json('success');

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $project = Project::find($index);

            $project->position = $newPosition;

            $project->save();
        }

        return response()->json('success');
    }
}
