<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CareerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $careers = Career::orderBy('position','asc')->get();
        return view('admin.pages.careers.index',compact('careers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.pages.careers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'start'=>'required',
            'end'=>'required',
            'description_ka'=>'required',
            'description_en'=>'required',

        ]);

        $data = $request->all();
        $data['deadline'] = $request->start.'|'.$request->end;
        $data['status'] = $request->status=='on'?1:0;
        Career::create($data);

        return redirect()->route('admin::careers.index')
            ->with('success','Career created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Career $career
     * @return Response
     */
    public function show(Career $career)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Career $career
     * @return Factory|View
     */
    public function edit(Career $career)
    {
        return view('admin.pages.careers.edit',compact('career'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Career $career
     * @return RedirectResponse
     */
    public function update(Request $request, Career $career)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'start'=>'required',
            'end'=>'required',
            'description_ka'=>'required',
            'description_en'=>'required',

        ]);

        $data = $request->all();
        $data['deadline'] = $request->start.'|'.$request->end;
        $data['status'] = $request->status=='on'?1:0;
        $career->update($data);

        return redirect()->route('admin::careers.index')
            ->with('success','Career Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Career $career
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Career $career)
    {
        $career->delete();
        return redirect()->route('admin::careers.index')
            ->with('success','Career Deleted successfully.');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $careers = Career::find($index);

            $careers->position = $newPosition;

            $careers->save();
        }

        return response()->json('success');
    }
}
