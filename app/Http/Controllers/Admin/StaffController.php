<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Staff;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $staffs = Staff::orderBy('position','asc')->get();
        return view('admin.pages.staff.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = Category::orderBy('position')->get();
        return view('admin.pages.staff.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ka'=>'required',
            'name_en'=>'required',
            'image'=>'required|mimes:jpg,jpeg,png,gif|max:2048',
            'job_ka'=>'required',
            'job_en'=>'required',
            'linkedin'=>'required',
            'category_id'=>'required'
        ]);


        $data = $request->all();
        $data['status'] = $request->status == "on"?1:0;

        //image upload
        $fileName = time().rand().'.'.$request->image->extension();
        $request->image->move(public_path('uploads'), $fileName);

        $data['image'] = $fileName;

        Staff::create($data);

        return redirect()->route('admin::staff.index')
            ->with('success','Staff created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Staff $staff
     * @return Response
     */
    public function show(Staff $staff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Staff $staff
     * @return Factory|View
     */
    public function edit(Staff $staff)
    {
        $categories = Category::orderBy('position')->get();
        return view('admin.pages.staff.edit',compact('staff','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Staff $staff
     * @return RedirectResponse
     */
    public function update(Request $request, Staff $staff)
    {
        $request->validate([
            'name_ka'=>'required',
            'name_en'=>'required',
            'image'=>'mimes:jpg,jpeg,png,gif|max:2048',
            'job_ka'=>'required',
            'job_en'=>'required',
            'linkedin'=>'required',
            'category_id'=>'required'
        ]);

        $data = $request->all();
        $data['status'] = $request->status == "on"?1:0;
        $data['main'] = $request->main=='on'?1:0;

        if ($request->file('image')){
            //image upload
            $fileName = time().'.'.$request->image->extension();
            $request->image->move(public_path('uploads'), $fileName);

            if(file_exists(public_path('uploads/'.$staff->image))){
                unlink(public_path('uploads/'.$staff->image));
            }

            $data['image'] = $fileName;
        }

        $staff->update($data);

        return redirect()->route('admin::staff.index')
            ->with('success','Staff Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Staff $staff
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Staff $staff)
    {
        if(file_exists(public_path('uploads/'.$staff->image))){
            unlink(public_path('uploads/'.$staff->image));
        }
        $staff->delete();

        return redirect()->route('admin::staff.index')
            ->with('success','Staff Deleted successfully.');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $staff = Staff::find($index);

            $staff->position = $newPosition;

            $staff->save();
        }

        return response()->json('success');
    }
}
