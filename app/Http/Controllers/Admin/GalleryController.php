<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class GalleryController extends Controller
{
    /**
     * GalleryController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $galleries = Gallery::orderBy('position','asc')->get();
        return view('admin.pages.gallery.index', compact('galleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.pages.gallery.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'category'=>'required',
            'image'=>'required_without:video|mimes:jpg,jpeg,png,svg,bmp | max:1000',
            'video'=>'required_without:image'
        ]);

        $gallery = new Gallery();
        $gallery->title_ka = $request->title_ka;
        $gallery->title_en = $request->title_en;

        if ($request->file('image')) {
            //image upload
            $fileName = time() . rand() . '.' . $request->image->extension();
            $request->image->move(public_path('uploads'), $fileName);

            $gallery->image = $fileName;
        }elseif (isset($request->video)){
            if (count(explode("v=",$request->video)) > 1){
                $videoUrl = explode("v=",$request->video)[1];
                $gallery->video = $videoUrl;
            }
        }
        $gallery->category = $request->category;

        $gallery->save();

        return redirect()->route('admin::galleries.index')
            ->with('success','Gallery created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Gallery $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Gallery $gallery
     * @return Factory|View
     */
    public function edit(Gallery $gallery)
    {
        return view('admin.pages.gallery.edit',compact('gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Gallery $gallery
     * @return RedirectResponse
     */
    public function update(Request $request, Gallery $gallery)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'image'=>'required_without:video|mimes:jpg,jpeg,png,svg,bmp | max:1000',
            'video'=>'required_without:image'
        ]);

        $gallery->title_ka = $request->title_ka;
        $gallery->title_en = $request->title_en;

        if ($request->file('image')) {
            //image upload
            $fileName = time() . rand() . '.' . $request->file->extension();
            $request->image->move(public_path('uploads'), $fileName);

            if(file_exists(public_path('uploads/'.$gallery->image))){
                unlink(public_path('uploads/'.$gallery->image));
            }

            $gallery->image = $fileName;
        }elseif (isset($request->video)){
            if (count(explode("v=",$request->video)) > 1){
                $videoUrl = explode("v=",$request->video)[1];
                $gallery->video = $videoUrl;
            }
        }
        $gallery->save();

        return redirect()->route('admin::galleries.index')
            ->with('success','Gallery updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Gallery $gallery
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Gallery $gallery)
    {
        if(file_exists(public_path('uploads/'.$gallery->file))){
            if (!empty($gallery->file)){
                unlink(public_path('uploads/'.$gallery->file));
            }
        }

        $gallery->delete();

        return redirect()->route('admin::galleries.index')
            ->with('success','Gallery Deleted Successfully.');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $gallery = Gallery::find($index);

            $gallery->position = $newPosition;

            $gallery->save();
        }

        return response()->json('success');
    }
}
