<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Slider;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $sliders = Slider::orderBy('position','asc')->get();
        return view('admin.pages.sliders.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.pages.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'address_ka'=>'required',
            'address_en'=>'required',
            'image'=>'required|mimes:png,jpg,jpeg,svg',
            'author_ka'=>'required',
            'author_en'=>'required',
        ]);


        $data = $request->all();
        $data['status'] = $request->status=='on'?1:0;

        //image upload
        $fileName = time().rand().'.'.$request->image->extension();
        $request->image->move(public_path('uploads'), $fileName);

        $data['image'] = $fileName;

        Slider::create($data);

        return redirect()->route('admin::sliders.index')
            ->with('success','Slider created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return Factory|View
     */
    public function edit(Slider $slider)
    {
        return view('admin.pages.sliders.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Slider $slider)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'address_ka'=>'required',
            'address_en'=>'required',
            'image'=>'mimes:png,jpg,jpeg,svg',
            'author_ka'=>'required',
            'author_en'=>'required',
        ]);


        $data = $request->all();
        $data['status'] = $request->status=='on'?1:0;

        //image upload
        if ($request->file('image')) {
            //image upload
            $fileName = time() . rand() . '.' . $request->image->extension();
            $request->image->move(public_path('uploads'), $fileName);

            if(file_exists(public_path('uploads/'.$slider->image))){
                unlink(public_path('uploads/'.$slider->image));
            }

            $data['image'] = $fileName;
        }

        $slider->update($data);

        return redirect()->route('admin::sliders.index')
            ->with('success','Slider Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Slider $slider
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Slider $slider)
    {
        if(file_exists(public_path('uploads/'.$slider->image))){
            unlink(public_path('uploads/'.$slider->image));
        }

        $slider->delete();

        return redirect()->route('admin::sliders.index')
            ->with('success','Service Deleted Successfully.');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $slider = Slider::find($index);

            $slider->position = $newPosition;

            $slider->save();
        }

        return response()->json('success');
    }
}
