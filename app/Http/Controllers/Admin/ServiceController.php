<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ServiceController extends Controller
{
    /**
     * ServiceController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $services = Service::orderBy('position','asc')->get();
        return view('admin.pages.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.pages.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'description_ka'=>'required',
            'description_en'=>'required',
            'icon'=>'required|mimes:png,ico'
        ]);

        $service = new Service();
        $service->title_ka = $request->title_ka;
        $service->title_en = $request->title_en;
        $service->description_ka = $request->description_ka;
        $service->description_en = $request->description_en;
        $service->status = $request->status == "on"?1:0;

        //image upload
        $fileName = time().rand().'.'.$request->icon->extension();
        $request->icon->move(public_path('uploads'), $fileName);

        $service->icon = $fileName;

        $service->save();

        return redirect()->route('admin::services.index')
            ->with('success','Service created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Service $service
     * @return void
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Service $service
     * @return Factory|View
     */
    public function edit(Service $service)
    {
        return view('admin.pages.services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Service $service
     * @return RedirectResponse
     */
    public function update(Request $request, Service $service)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'description_ka'=>'required',
            'description_en'=>'required',
            'icon'=>'mimes:png,ico'
        ]);

        $service->title_ka = $request->title_ka;
        $service->title_en = $request->title_en;
        $service->description_ka = $request->description_ka;
        $service->description_en = $request->description_en;
        $service->status = $request->status == "on"?1:0;

        if ($request->file('icon')) {
            //image upload
            $fileName = time() . rand() . '.' . $request->icon->extension();
            $request->icon->move(public_path('uploads'), $fileName);

            if(file_exists(public_path('uploads/'.$service->icon))){
                unlink(public_path('uploads/'.$service->icon));
            }

            $service->icon = $fileName;
        }

        $service->save();

        return redirect()->route('admin::services.index')
            ->with('success','Service updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Service $service
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Service $service)
    {
        if(file_exists(public_path('uploads/'.$service->icon))){
            unlink(public_path('uploads/'.$service->icon));
        }

        $service->delete();

        return redirect()->route('admin::services.index')
            ->with('success','Service Deleted Successfully.');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $service = Service::find($index);

            $service->position = $newPosition;

            $service->save();
        }

        return response()->json('success');
    }
}
