<?php

namespace App\Http\Controllers\Admin;

use App\Menu;
use App\Page;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $menus = Menu::orderBy('position','asc')->get();
        $pages = Page::orderBy('position','asc')->where('status',1)->get();


        return view('admin.pages.menus.index',compact('menus','pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title_ka'=>'required',
            'title_en'=>'required',
            'slug'=>'required',
        ]);

        $menu = new Menu();
        $menu->title_ka = $request->title_ka;
        $menu->title_en = $request->title_en;
        $menu->slug = $request->slug;
        if ($request->parent_id){
            $menu->parent_id = $request->parent_id;
        }
        $menu->save();
        return back()->with('success','Menu item added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Menu  $menu
     * @return Response
     */
    public function update(Request $request, Menu $menu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Menu $menu
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return back()->with('success','menu removed successfully');
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $menu = Menu::find($index);

            $menu->position = $newPosition;

            $menu->save();
        }

        return response()->json('success');
    }
}
