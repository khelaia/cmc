<?php

namespace App\Http\Controllers\Admin;

use App\About;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AboutController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Factory|View
     */
    public function index(){
        $about = About::find(1);
        return view('admin.pages.about.index',compact('about'));
    }

    /**
     * @param Request $request
     * @param About $about
     * @return Factory|View
     */
    public function update(Request $request){
        $request->validate([
            'description_ka'=>'required',
            'description_en'=>'required'
        ]);
        $about = About::find(1);
        $about->description_ka = $request->description_ka;
        $about->description_en = $request->description_en;
        $about->save();

        return back()->with('success','About Updated Successfully');
    }
}
