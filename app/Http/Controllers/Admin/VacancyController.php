<?php

namespace App\Http\Controllers\Admin;

use App\Career;
use App\Http\Controllers\Controller;
use App\Vacancy;
use Illuminate\Http\Request;
use DataTables;

class VacancyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request){
        if ($request->ajax()) {
            $data = Vacancy::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('career_id',function ($row){
                    $career = Career::findOrFail($row->career_id);
                    return $career->title_ka;
                })
                ->addColumn('file',function ($row){
                    $btn = '<a target="_blank" href="/uploads/'.$row->file.'">'.$row->file.'</a>';
                    return $btn;
                })

                ->rawColumns(['file'])
                ->make(true);
        }

        return view('admin.pages.vacancy.index');
    }
}
