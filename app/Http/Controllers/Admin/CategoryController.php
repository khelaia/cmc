<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $categories = Category::orderBy('position','asc')->get();
        return view('admin.pages.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('admin.pages.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_ka'=>'required',
            'name_en'=>'required'
        ]);

        $category = new Category();

        $category->name_ka = $request->name_ka;
        $category->name_en = $request->name_en;

        $category->save();

        return redirect()->route('admin::categories.index')
            ->with('success','Category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Category $category
     * @return Factory|View
     */
    public function edit(Category $category)
    {
        return view('admin.pages.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name_ka'=>'required',
            'name_en'=>'required'
        ]);

        $category->name_ka = $request->name_ka;
        $category->name_en = $request->name_en;

        $category->save();

        return redirect()->route('admin::categories.index')
            ->with('success','Category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return redirect()->route('admin::categories.index')
            ->with('success','Category Deleted Successfully.');
    }
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function position_update(Request $request){
        $request->validate([
            'positions'=>'required'
        ]);

        foreach ($request->positions as $position){
            $index = $position[0];
            $newPosition = $position[1];
            $category = Category::find($index);

            $category->position = $newPosition;

            $category->save();
        }

        return response()->json('success');
    }
}
