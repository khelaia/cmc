<?php

namespace App\Http\Controllers\User;

use App\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $about = About::findOrFail(1);
        return view('user.pages.about',compact('about','locale'));
    }
}
