<?php

namespace App\Http\Controllers\User;

use App\Page;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $seo = Page::where('slug','services')->first();
        $services = Service::orderBy('position','asc')->where('status',1)->get();
        return view('user.pages.services',compact('services', 'locale','seo'));
    }
}
