<?php

namespace App\Http\Controllers\User;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $images = Gallery::orderBy('position','asc')->where('category','=','image')->limit(8)->get();
        $videos = Gallery::orderBy('position','asc')->where('category','=','video')->limit(8)->get();

        $imagesList =[];
        foreach ($images as $image){
            $imagesList[]=$image;
        }
        $videosList = [];
        foreach ($videos as $video){
            $videosList[]=$video;
        }

        $images = array_chunk($imagesList,2);
        $images = array_chunk($images,2);
        $videos = array_chunk($videosList,2);
        $videos = array_chunk($videos,2);

        return view('user.pages.gallery',compact('locale','images','videos'));
    }

    public function video($locale){
        app()->setLocale($locale);
        $videos = Gallery::orderBy('position','asc')->where('category','=','video')->get();
        $videosList = [];
        foreach ($videos as $video){
            $videosList[]=$video;
        }
        $videos = array_chunk($videosList,2);
        $videos = array_chunk($videos,2);

        return view('user.pages.video',compact('locale','videos'));
    }

    public function image($locale){
        app()->setLocale($locale);
        $images = Gallery::orderBy('position','asc')->where('category','=','image')->get();
        $imagesList =[];
        foreach ($images as $image){
            $imagesList[]=$image;
        }
        $images = array_chunk($imagesList,2);
        $images = array_chunk($images,2);

        return view('user.pages.image',compact('locale','images'));
    }
}
