<?php

namespace App\Http\Controllers\User;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $clients = Client::orderBy('position','asc')->where('status',1)->get();
        return view('user.pages.clients',compact('clients', 'locale'));
    }
}
