<?php

namespace App\Http\Controllers\User;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $contact = Contact::findOrFail(1);
        return view('user.pages.contact', compact('locale','contact'));
    }
}
