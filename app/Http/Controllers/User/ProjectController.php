<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\ProjectResource;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $total = Project::where('status',1)->count();
        return view('user.pages.projects',compact('total','locale'));
    }

    public function getProjects(Request $request){
        $projects = Project::orderBy('position','asc')->where('status','=',1);

        if ($request->has('status')){
            $projects->where('status_en','=',$request->status);
        }
        if ($request->has('take')){
            $projects->limit($request->take);
        }
        if ($request->has('main')){
            if($request->get('main')=='true'){
                $projects->where('main','=',1);
            }
        }

        if ($request->has('pretty')){
            if ($request->pretty=='true'){
                $projectsList = [];
                foreach ($projects->get() as $project){
                    $projectsList[]=$project;
                }

                $projects = array_chunk($projectsList,2);
                $projects = array_chunk($projects,2);
                return response()->json($projects);
            }

        }
        return ProjectResource::collection($projects->get());
    }


    public function show($locale,$slug){
        app()->setLocale($locale);
        $project = Project::where('slug','=',$slug)->where('status',1)->get();
        $project = $project[0];
        return view('user.pages.project',compact('project','locale'));
    }
}
