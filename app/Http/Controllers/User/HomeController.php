<?php

namespace App\Http\Controllers\User;

use App\Client;
use App\Service;
use App\Slider;
use App\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $services = Service::orderBy('position','asc')->where('status',1)->limit(12)->get();
        $clients = Client::orderBy('position','asc')->where('status',1)->limit(12)->get();
        $sliders = Slider::orderBy('position','asc')->where('status',1)->limit(8)->get();
        $teams = Staff::orderBy('position','asc')->where('status',1)->where('main',1)->limit(8)->get();

        $teamList =[];
        foreach ($teams as $team){
            $teamList[]=$team;
        }
        $teams = array_chunk($teamList,2);
        return view('user.pages.home',compact('services','clients','sliders','teams','locale'));
    }
}
