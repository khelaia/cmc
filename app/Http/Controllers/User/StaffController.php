<?php

namespace App\Http\Controllers\User;

use App\Category;
use App\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $categories = Category::orderBy('position','asc')->get();
        return view('user.pages.team',compact('categories','locale'));
    }
}
