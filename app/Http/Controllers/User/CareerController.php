<?php

namespace App\Http\Controllers\User;

use App\Career;
use App\Vacancy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CareerController extends Controller
{
    public function index($locale){
        app()->setLocale($locale);
        $careers = Career::orderBy('position','asc')->where('status',1)->get();
        return view('user.pages.career',compact('careers','locale'));
    }

    public function store(Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'file'=>'required|max:2110|mimes:pdf,docx,doc'
        ]);

        $data = $request->all();
        $fileName = time() . rand() . '.' . $request->file->extension();
        $request->file->move(public_path('uploads'), $fileName);

        $data['file'] = $fileName;

        Vacancy::create($data);

        return back()->with('success','Sent Successfully');
    }
}
