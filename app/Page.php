<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title_ka','title_en','keywords_ka','keywords_en','description_ka','description_en','status'];
}
