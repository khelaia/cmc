<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $fillable = [
        'name_ka',
        'name_en',
        'image',
        'job_ka',
        'job_en',
        'linkedin',
        'category_id',
        'status','main'
    ];


    public function category(){
        return $this->belongsTo(Category::class);
    }
}
