<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['title_ka','title_en','address_ka','address_en','author_ka','author_en','url','image','status'];
}
