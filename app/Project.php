<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'title_ka',
        'title_en',
        'cover',
        'gallery',
        'budget_ka',
        'budget_en',
        'location_ka',
        'location_en',
        'area_ka',
        'area_en',
        'status_ka',
        'status_en',
        'scope_ka',
        'scope_en',
        'customer_ka',
        'customer_en',
        'description_ka',
        'description_en',
        'table_ka',
        'table_en',
        'keywords_ka',
        'keywords_en',
        'status',
        'main',
        'slug'
    ];
}
