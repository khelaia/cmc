<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_ka');
            $table->string('name_en');
            $table->string('image');
            $table->string('job_ka');
            $table->string('job_en');
            $table->string('linkedin')->nullable();
            $table->integer('category_id')->unsigned();
            $table->boolean('status')->default(1);
            $table->boolean('main')->default(0);
            $table->string('position')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
