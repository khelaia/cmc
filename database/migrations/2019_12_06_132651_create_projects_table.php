<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_ka');
            $table->string('title_en');
            $table->string('cover');
            $table->text('gallery')->nullable();
            $table->string('budget_ka')->nullable();
            $table->string('budget_en')->nullable();
            $table->string('location_ka')->nullable();
            $table->string('location_en')->nullable();
            $table->string('area_ka')->nullable();
            $table->string('area_en')->nullable();
            $table->string('status_ka')->nullable();
            $table->string('status_en')->nullable();
            $table->string('scope_ka')->nullable();
            $table->string('scope_en')->nullable();
            $table->string('customer_ka')->nullable();
            $table->string('customer_en')->nullable();
            $table->text('description_ka')->nullable();
            $table->text('description_en')->nullable();
            $table->text('keywords_ka')->nullable();
            $table->text('keywords_en')->nullable();
            $table->string('slug');
            $table->boolean('status')->default(1);
            $table->boolean('main')->default(0);
            $table->integer('position')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
