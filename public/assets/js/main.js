// var services = [{
//     "title": "Test 7",
//     "category": "ongoing"
//   },
//   {
//     "title": "Test 2",
//     "category": "ongoing"
//   },
//   {f
//     "title": "Test 2",
//     "category": "ongoing"
//   },
//   {
//     "title": "Lorem Ipsum",
//     "category": "completed"
//   },
//   {
//     "title": "Lorem Ipsum",
//     "category": "completed"
//   },
//   {
//     "title": "Lorem Ipsum",
//     "category": "completed"
//   },
//   {
//     "title": "Lorem Ipsum",
//     "category": "completed"
//   },
//   {
//     "title": "Lorem Ipsum",
//     "category": "completed"
//   },
// ]

// var projects = "";
// for (var i = 0; i < services.length; i++) {
//   projects += '<div id="div-one" class="p_t_left project ongoing" style="background-image: url(images/project.png)"><img src="images/projectblur.png" alt=""><div class="projects_info"><h4>' + services[i]["title"] + '</h4><span><img src="images/location.png" alt=""> TBILISI, REPUBLIC OF GEORGIA</span></div></div>'
// }

// $('.projects_top').append(projects);

// $('#grid-filter button').click(function () {
//   var projects = ""
//   var orderBy = $(this).attr('data-filter');
//   for (var i = 0; i < services.length; i++) {
//     if (orderBy == "*") {
//       projects += '<div id="div-one" class="p_t_left project ongoing" style="background-image: url(images/project.png)"><img src="images/projectblur.png" alt=""><div class="projects_info"><h4>' + services[i]["title"] + '</h4><span><img src="images/location.png" alt=""> TBILISI, REPUBLIC OF GEORGIA</span></div></div>'
//     } else {
//       if (services[i]["category"] == orderBy) {
//         projects += '<div id="div-one" class="p_t_left project ongoing" style="background-image: url(images/project.png)"><img src="images/projectblur.png" alt=""><div class="projects_info"><h4>' + services[i]["title"] + '</h4><span><img src="images/location.png" alt=""> TBILISI, REPUBLIC OF GEORGIA</span></div></div>'
//       }
//     }

//   }
//   $('.projects_top').empty();
//   $('.projects_top').append(projects);

// });

// $(document).ready(function () {
//   var $container = $('.projects_top');
//   // filter buttons
//   $('#grid-filter button').click(function () {
//     var $this = $(this);
//     // don't proceed if already selected
//     if (!$this.hasClass('is-checked')) {
//       $this.parents('#options').find('.is-checked').removeClass('is-checked');
//       $this.addClass('is-checked');
//     }
//     var selector = $this.attr('data-filter');
//     console.log(selector);
//     $container.isotope({
//       itemSelector: '.project',
//       filter: selector
//     });
//     return false;
//   });

// });


// $("#one").click(function () {
//   myArray = []
//   var i = 
//   $('.project').filter(function() {
//     return $(this).css('display') != 'none';
//   });  
//   console.log(i);
// });



//http://www.epicwebs.co.uk/jquery-tutorials/quick-and-easy-jquery-masonry-tutorial/
$('#masonry-grid').masonry({
  columnWidth: 585,
  itemSelector: '.grid-item'
});



$("#grid-filter li").click(function () {
  var group = $(this).data('category');
  var group_class = "." + group;

  if (group == "*") {
    $(".grid-item").show();
    $('#masonry-grid').masonry('layout');
  } else if (group != "") {
    $(".grid-item").hide();
    $(group_class).show();
    $('#masonry-grid').masonry('layout');
  } else {
    $(".grid-item").show();
    $('#masonry-grid').masonry('layout');
  }
});



//Scroll
$(".up").on('click', function () {
  $('html, body').animate({
    scrollTop: $("#header_fluid").offset().top - 50
  }, 800);
})


//Team Carousel

$('.owl-carousel').owlCarousel({
  items: 4,
  loop: false,
  center: true,
  margin: 0,
  URLhashListener: true,
  autoplayHoverPause: true,
  startPosition: 'team'
});

//Project Carousel

var swiper = new Swiper('.swiper-container', {
  slidesPerView: 3,
  // loop: true,
  centeredSlides: true,
  spaceBetween: 10,
  pagination: {
    clickable: true,
  },
});


//Team Carousel

var swiper = new Swiper('.swiper-container3', {
  slidesPerView: 3,
  // centeredSlides: true,
  spaceBetween: 15,
  pagination: {
    // el: '.swiper-pagination',
    clickable: true,
  },
});

//Main Carousel 

var swiper = new Swiper('.swiper-container2', {
  loop: true,
  fadeEffect: {
    crossFade: true
  },
  effect: 'fade',
  pagination: {
    el: '.swiper-pagination',
    type: 'fraction',
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});


new WOW().init();



// Clients Carousel

var swiper = new Swiper('.swiper-container4', {
  slidesPerView: 5,
  centeredSlides: true,
  spaceBetween: 30,
  pagination: {
    // el: '.swiper-pagination',
    clickable: true,
  },
});

// WhoWeAre Carousel

var swiper = new Swiper('.swiper-container5', {
  slidesPerView: 2,
  // loop:true,
  centeredSlides: true,
  spaceBetween: 10,
  pagination: {
    // el: '.swiper-pagination',
    clickable: true,
  },
});


//Project Carousel 

var swiper = new Swiper('.swiper-container6', {
  loop: true,
  fadeEffect: {
    crossFade: true
  },
  effect: 'fade',
  pagination: {
    el: '.swiper-pagination',
    type: 'fraction',
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});


// Videos Carousel

var swiper = new Swiper('.swiper-container7', {
  slidesPerView: 2,
  // loop:true,
  centeredSlides: true,
  spaceBetween: 11,
  pagination: {
    // el: '.swiper-pagination',
    clickable: true,
  },
});


//Button click

$(".card-header").on('click', function () {
  
  $(this).find("img").toggleClass("active")

})

//POPUP GALLERY

$(document).ready(function() {
	$('.zoom-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
			}
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}
		
	});
});

//pop video

$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});


// MENU

$(".menuBar").click(function () {
  $(".smMenuSlide").addClass("active")
})


$(".closeMenu").click(function () {
  $(".smMenuSlide").removeClass("active")
})