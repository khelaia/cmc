<?php $__env->startSection('content'); ?>
    <div class="gallery_container">


        <?php echo $__env->make('user.includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


    </div>

    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots_top">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="">Home</a>
                <a href="" class="title_arrows">Projects</a>
            </div>
            <div class="page_fluid_text fluid_flexible">
                <h1>Projects</h1>
                <div id="grid-filter" class="projects_filters">

                    <button id="all" class="button is-checked" data-filter="*">ALL</button>
                    <button id="one" class="button" data-filter="ongoing">Ongoing</button>
                    <button id="two" class="button" data-filter="completed">Complited</button>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="projects_main_inside">
                <div class="projects_inside_top">
                    <div class="pit_left">
                        <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                            <img src="/assets/images/projectblur.png" alt="">
                            <div class="projects_info">
                                <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                    REPUBLIC OF
                                    GEORGIA</span>
                            </div>
                        </div>
                    </div>
                    <div class="pit_right">
                        <div class="projects_in_block" style="background-image: url(/assets/images/project1.png)">
                            <img src="/assets/images/projectblur.png" alt="">
                            <div class="projects_info">
                                <h4>UZNADZE LIVING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                    REPUBLIC OF
                                    GEORGIA</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="projects_inside_bottom">
                    <div class="pib_left">
                        <div class="projects_in_block" style="background-image: url(/assets/images/project2.png)">
                            <img src="/assets/images/projectblur.png" alt="">
                            <div class="projects_info">
                                <h4>PROSECUTOR’S OFFICE</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                    REPUBLIC OF
                                    GEORGIA</span>
                            </div>
                        </div>
                    </div>
                    <div class="pib_right">
                        <div class="projects_in_block" style="background-image: url(/assets/images/project3.png)">
                            <img src="/assets/images/projectblur.png" alt="">
                            <div class="projects_info">
                                <h4>MERCEDES SHOPING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                    REPUBLIC OF
                                    GEORGIA</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="projects_inside_top">
                    <div class="pit_left">
                        <div class="projects_in_block" style="background-image: url(/assets/images/project4.png)">
                            <img src="/assets/images/projectblur.png" alt="">
                            <div class="projects_info">
                                <h4>SHOPPING MALL MERANI</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                    REPUBLIC OF
                                    GEORGIA</span>
                            </div>
                        </div>
                    </div>
                    <div class="pit_right">
                        <div class="projects_in_block" style="background-image: url(/assets/images/project5.png)">
                            <img src="/assets/images/projectblur.png" alt="">
                            <div class="projects_info">
                                <h4>Le Meridien </h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                    REPUBLIC OF
                                    GEORGIA</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="projects_load_more">
                <button type="submit" class="btn btn-primary">LOAD MORE <img src="/assets/images/pageright.png"
                                                                             alt=""></button> </div>
        </div>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/user/pages/projects.blade.php ENDPATH**/ ?>