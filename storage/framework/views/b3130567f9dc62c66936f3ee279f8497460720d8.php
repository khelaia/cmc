<header id="header_fluid">
    <div class="space">

    </div>
    <div class="main_logo">
        <a href="/"><img src="<?php echo e(asset('assets/images/logo.png')); ?>" alt=""></a>
    </div>
    <div class="main_nav">

        <div class="main_menu">
            <ul>
                <li class="haschild"><a href="/whoweare">WHO WE ARE</a></li>
                <li class="haschild"><a href="">WHAT WE DO</a></li>
                <li><a href="/clients">CLIENTS</a></li>
                <li><a href="/projects">PROJECTS</a></li>
                <li><a href="">CONTACTS</a></li>
            </ul>
        </div>

    </div>
    <div class="language_nav">
        <span class="active">ENG</span>
        <span>GEO</span>
    </div>

</header>
<?php /**PATH C:\Users\User\Desktop\cmc\resources\views/user/includes/header.blade.php ENDPATH**/ ?>