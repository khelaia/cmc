<?php $__env->startSection('content'); ?>
    <div class="members_container">


        <?php echo $__env->make('user.includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="about_pages_blur">
            <img src="/assets/images/aboublur.png" alt="">
        </div>

        <div class="about_page_share">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>

    </div>

    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="">Home</a>
                <a href="" class="title_arrows">Who We Are</a>
            </div>
            <div class="page_fluid_text">
                <h1>Who We Are</h1>
            </div>
        </div>
        <div class="who_we_are">
            <div class="container">
                <p>In construction business «coordination and management» service seems to be already the commonly
                    used practice in many leading countries all over the world. While Monitoring and arranging the
                    organizational, financial and technical support to develop relationships between a customer and
                    a provider the service significantly reduces the costs and lead time for a client.
                </p>
                <p> It was in 2009 when “CMC” (Construction Management & Consulting) introduced the practice in
                    Georgia tailored to the country specifics. It proved to be a huge success having resulted in
                    over 50 large scale projects since then. The aggregate value of the projects implemented by the
                    company exceeds 728 mln USD and covers more than 676 000 square meters. As a result, today the
                    company is well experienced, most trusted and reliable partner providing regional construction
                    consultation and management. The company is characterized to handle and implement any difficult
                    project within the given time and budget while ensuring high quality.
                </p>
                <p>CMC covers a broad range of sectors, including civil (governmental) projects, commercial,
                    industrial, residential, retail, hospitality and mixed-use. CMC has built international networks
                    of service providers, vendors and distributors, so that the local projects can be completed to
                    the highest standards on time. Company has been commissioned on a variety of schemes at all
                    stages of the project lifecycle, from the design development to the implementation of
                    construction projects varying in size and complexity. Despite significant challenges projects
                    always have been delivered on time, in style and on budget.
                </p>
                <p>CMC has experience working with international contracts such as FIDIC books and EPC Turnkey
                    contracts. Company has Project Implementation Support, Engineering Design Services and
                    supervision services in various donor-funded projects, including specific experience in
                    Procurement policies and rules of international financial institutions such as EBRD, OPIC and
                    IFC.
                </p>
                <p>CMC has experience working with international contracts such as FIDIC books and EPC Turnkey
                    contracts. Company has Project Implementation Support, Engineering Design Services and
                    supervision services in various donor-funded projects, including specific experience in
                    Procurement policies and rules of international financial institutions such as EBRD, OPIC and
                    IFC.
                </p>
                <p>CMC was founded in 2009. Only 4 people were involved as its founding startup team. But as the
                    company grew, it added group of professionals, who were able to effectively optimize the quality
                    of a high-performance building projects and cope with various difficulties. Since starting CMC
                    has reached from 4 to 40 employees. All of them are very experienced and highly qualified, who
                    have been engaged in the process of construction of the complex projects, all of them share a
                    common drive of success and an enthusiasm to their work. Working at CMC has given to many of our
                    employees the opportunity to work on the most iconic and challenging projects across Georgia.
                </p>
            </div>
        </div>
        <div class="projects_in_slider">
            <div class="container">
                <div class="col-md-12">
                    <div class="projects_in_title">
                        <h3>Hightlighted projects</h3>
                        <div class="projects_full">
                            <span><a href="">ALL PROJECTS</a></span>
                            <a href=""><img src="/assets/images/seeallbg.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="projects_slider_row">
                <div class="swiper-container5">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="projects_in_column">
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="projects_in_column">
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="projects_in_column">
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                                <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                    <img src="/assets/images/projectblur.png" alt="">
                                    <div class="projects_info">
                                        <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt="">
                                            TBILISI, REPUBLIC OF
                                            GEORGIA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider_line"></div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/user/pages/about.blade.php ENDPATH**/ ?>