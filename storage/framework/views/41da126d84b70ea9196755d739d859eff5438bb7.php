<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-header row"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="horz-layout-basic">Slider Edit</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collpase show">
                                    <div class="card-body">
                                        <div class="card-text">
                                            <p></p>
                                        </div>
                                        <?php if($errors->any()): ?>
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li><?php echo e($error); ?></li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        <?php endif; ?>
                                        <form class="form form-horizontal" method="POST" action="<?php echo e(route('admin::sliders.update', $slider->id)); ?>" enctype="multipart/form-data">
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('PUT'); ?>
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-clipboard"></i>Slider</h4>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Title KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Title KA" name="title_ka" value="<?php echo e($slider->title_ka); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Title EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Title EN" name="title_en" value="<?php echo e($slider->title_en); ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control">Image</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <label id="projectinput8" class="file center-block">
                                                            <input name="image" type="file" id="file">
                                                            <span class="file-custom"></span>
                                                            <img style="height: 70px;object-fit: cover" src="/uploads/<?php echo e($slider->image); ?>">
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Address KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Address KA" name="address_ka" value="<?php echo e($slider->address_ka); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Address EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Address EN" name="address_en" value="<?php echo e($slider->address_en); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Author KA</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Author KA" name="author_ka" value="<?php echo e($slider->author_ka); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">Author EN</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="text" id="projectinput5" class="form-control" placeholder="Author EN" name="author_en" value="<?php echo e($slider->author_en); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-md-3 label-control" for="projectinput5">URL</label>
                                                    <div class="col-md-9 mx-auto">
                                                        <input type="url" id="projectinput5" class="form-control" placeholder="URL" name="url" value="<?php echo e($slider->url); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-9 mx-auto">
                                                        <input name="status" type="checkbox" id="switcheryColor4" class="switchery" data-color="success" <?php if($slider->status): ?> checked <?php endif; ?>/>
                                                        <label for="switcheryColor4" class="card-title ml-1">Published</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/material-vendors.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/switchery.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/plugins/forms/switch.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/fonts/simple-line-icons/style.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/colors/palette-switch.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>


    <!-- BEGIN: Theme JS-->
    <script src="<?php echo e(asset('app-assets/js/core/app-menu.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/core/app.js')); ?>"></script>
    <!-- END: Theme JS-->

    <!-- END: Page JS-->
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/switchery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')); ?>"></script>

    <script src="<?php echo e(asset('app-assets/js/scripts/pages/material-app.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/switch.js')); ?>"></script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/admin/pages/sliders/edit.blade.php ENDPATH**/ ?>