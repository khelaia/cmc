<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-header row"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section id="horizontal-form-layouts">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title" id="horz-layout-basic">About Info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="card-content collpase show">
                                    <div class="card-body">
                                        <div class="card-text">
                                            <p></p>
                                        </div>
                                        <?php if($message = Session::get('success')): ?>
                                            <div class="alert alert-success">
                                                <p><?php echo e($message); ?></p>
                                            </div>
                                        <?php endif; ?>
                                        <?php if($errors->any()): ?>
                                            <div class="alert alert-danger">
                                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                <ul>
                                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <li><?php echo e($error); ?></li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </ul>
                                            </div>
                                        <?php endif; ?>
                                        <form class="form form-horizontal" method="POST" action="<?php echo e(route('admin::about.update')); ?>" >
                                            <?php echo csrf_field(); ?>
                                            <?php echo method_field('PUT'); ?>
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-clipboard"></i>About</h4>

                                                <div class="form-group row">
                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset class="form-group">
                                                            <textarea name="description_ka" class="form-control" id="tiny_ka" rows="3" placeholder="Description KA"><?php echo e($about->description_ka); ?></textarea>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="form-group row">

                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset class="form-group">
                                                            <textarea name="description_en" class="form-control" id="tiny_en" rows="3" placeholder="Description EN"><?php echo e($about->description_en); ?></textarea>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-actions">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                    <i class="la la-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/material-vendors.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/switchery.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/plugins/forms/switch.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/fonts/simple-line-icons/style.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/colors/palette-switch.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>

    <script src="<?php echo e(asset('tinymce/tinymce.js')); ?>"></script>
    <script>
        tinymce.init({
            selector:'textarea#tiny_en',
            height:500,
            plugins: "paste",
            paste_as_text: true,
        });
        tinymce.init({
            selector:'textarea#tiny_ka',
            height:500,
            plugins: "paste",
            paste_as_text: true,


        });


    </script>

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo e(asset('app-assets/vendors/js/extensions/dropzone.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/ui/prism.min.js')); ?>"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo e(asset('app-assets/js/core/app-menu.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/core/app.js')); ?>"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo e(asset('app-assets/js/scripts/pages/material-app.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/extensions/dropzone.js')); ?>"></script>
    <!-- END: Page JS-->
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/switchery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')); ?>"></script>

    <script src="<?php echo e(asset('app-assets/js/scripts/pages/material-app.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/switch.js')); ?>"></script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/admin/pages/about/index.blade.php ENDPATH**/ ?>