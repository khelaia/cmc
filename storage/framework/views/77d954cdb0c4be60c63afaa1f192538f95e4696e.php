<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-header row">
        </div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section id="horizontal-form-layouts">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title" id="horz-layout-basic">Project Info</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="card-content collpase show">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <p></p>
                                            </div>
                                            <?php if($errors->any()): ?>
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <li><?php echo e($error); ?></li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                            <form class="form form-horizontal" method="POST" action="<?php echo e(route('admin::projects.store')); ?>" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>
                                                <div class="form-body">
                                                    <h4 class="form-section"><i class="ft-clipboard"></i>Project</h4>

                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Title KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Title KA" name="title_ka">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Title EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Title EN" name="title_en">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control">Cover Image</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <label id="projectinput8" class="file center-block">
                                                                <input name="cover" type="file" id="file">
                                                                <span class="file-custom"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Budget KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Budget KA" name="budget_ka">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Budget EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Budget EN" name="budget_en">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Location KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Location KA" name="location_ka">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Location EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Location EN" name="location_en">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Area KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Area KA" name="area_ka">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Area EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Area EN" name="area_en">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Status KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Status KA" name="status_ka">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Status EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Status EN" name="status_en">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Scope KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Scope KA" name="scope_ka">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Scope EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Scope EN" name="scope_en">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Customer KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Customer KA" name="customer_ka">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Customer EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Customer EN" name="customer_en">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <fieldset class="form-group">
                                                                <textarea name="description_ka" class="form-control" id="tiny_ka" rows="3" placeholder="Description KA"></textarea>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <fieldset class="form-group">
                                                                <textarea name="description_en" class="form-control" id="tiny_en" rows="3" placeholder="Description EN"></textarea>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-2 file-repeater">
                                                        <div class="col-md-9 mx-auto">
                                                            <div class="card-header">
                                                                <h4 class="card-title">Gallery Multiple Images</h4>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="card-body">
                                                                    <fieldset class="form-group">
                                                                        <div class="custom-file">
                                                                            <input name="gallery[]" type="file" class="custom-file-input" id="inputGroupFile02" multiple="multiple">
                                                                            <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Choose file</label>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <input name="status" type="checkbox" id="switcheryColor4" class="switchery" data-color="success" checked />
                                                            <label for="switcheryColor4" class="card-title ml-1">Published</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group row">
                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset>
                                                            <label>SEO - Google Keywords KA</label>
                                                            <div class="form-group">
                                                                <div id="keywordsKa" class="edit-on-delete form-control" data-tags-input-name="keywords_ka"></div>
                                                                <p class="text-muted mt-1">You can write some keywords for SEO</p>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-9 mx-auto">
                                                        <fieldset>
                                                            <label>SEO - Google Keywords EN</label>
                                                            <div class="form-group">
                                                                <div id="keywordsEn" class="edit-on-delete form-control" data-tags-input-name="keywords_en"></div>
                                                                <p class="text-muted mt-1">You can write some keywords for SEO</p>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>


                                                <div class="form-actions">

                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/material-vendors.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/switchery.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/plugins/forms/switch.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/fonts/simple-line-icons/style.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/colors/palette-switch.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('app-assets/vendors/css/forms/tags/tagging.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <script src="<?php echo e(asset('tinymce/tinymce.js')); ?>"></script>
    <script>
        tinymce.init({
            selector:'textarea#tiny_en',
            height:500,
            paste_as_text: true,
            plugins:"paste",


        });
        tinymce.init({
            selector:'textarea#tiny_ka',
            height:500,
            plugins: 'paste',
            paste_as_text: true

        });
    </script>

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo e(asset('app-assets/vendors/js/extensions/dropzone.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/ui/prism.min.js')); ?>"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo e(asset('app-assets/js/core/app-menu.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/core/app.js')); ?>"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo e(asset('app-assets/js/scripts/pages/material-app.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/extensions/dropzone.js')); ?>"></script>
    <!-- END: Page JS-->
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/switchery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')); ?>"></script>

    <script src="<?php echo e(asset('app-assets/js/scripts/pages/material-app.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/switch.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/form-repeater.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/tags/tagging.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/tags/tagging.js')); ?>"></script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/admin/pages/projects/create.blade.php ENDPATH**/ ?>