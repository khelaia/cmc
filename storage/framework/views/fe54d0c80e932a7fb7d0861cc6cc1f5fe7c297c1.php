<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-header row"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section id="horizontal-form-layouts">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title" id="horz-layout-basic">Project Info</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="card-content collpase show">
                                        <div class="card-body">
                                            <div class="card-text">
                                                <p></p>
                                            </div>
                                            <?php if($errors->any()): ?>
                                                <div class="alert alert-danger">
                                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                                    <ul>
                                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <li><?php echo e($error); ?></li>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                            <form class="form form-horizontal" method="POST" action="<?php echo e(route('admin::projects.update',$project->id)); ?>" enctype="multipart/form-data">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('PUT'); ?>
                                                <div class="form-body">
                                                    <h4 class="form-section"><i class="ft-clipboard"></i>Edit Project</h4>

                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Title KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Title KA" name="title_ka" value="<?php echo e($project->title_ka); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Title EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Title EN" name="title_en" value="<?php echo e($project->title_en); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control">Cover Image</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <label id="projectinput8" class="file center-block">
                                                                <input name="cover" type="file" id="file">
                                                                <span class="file-custom"></span>
                                                            </label>
                                                            <span class="media-left avatar-online">
                                                                <img class="media-object" src="/uploads/<?php echo e($project->cover); ?>"  style="height: 64px; object-fit: cover">
                                                                <i></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Budget KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Budget KA" name="budget_ka" value="<?php echo e($project->budget_ka); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Budget EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Budget EN" name="budget_en" value="<?php echo e($project->budget_en); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Location KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Location KA" name="location_ka" value="<?php echo e($project->location_ka); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Location EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Location EN" name="location_en" value="<?php echo e($project->location_en); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Area KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Area KA" name="area_ka" value="<?php echo e($project->area_ka); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Area EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Area EN" name="area_en" value="<?php echo e($project->area_en); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Status KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Status KA" name="status_ka" value="<?php echo e($project->status_ka); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Status EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Status EN" name="status_en" value="<?php echo e($project->status_en); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Scope KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Scope KA" name="scope_ka" value="<?php echo e($project->scope_ka); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Scope EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Scope EN" name="scope_en" value="<?php echo e($project->scope_en); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Customer KA</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Customer KA" name="customer_ka" value="<?php echo e($project->customer_ka); ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-md-3 label-control" for="projectinput5">Customer EN</label>
                                                        <div class="col-md-9 mx-auto">
                                                            <input type="text" id="projectinput5" class="form-control" placeholder="Customer EN" name="customer_en" value="<?php echo e($project->customer_en); ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <fieldset class="form-group">
                                                                <textarea name="description_ka" class="form-control" id="tiny_ka" rows="3" placeholder="Description KA"><?php echo e($project->description_ka); ?></textarea>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <fieldset class="form-group">
                                                                <textarea name="description_en" class="form-control" id="tiny_en" rows="3" placeholder="Description EN"><?php echo e($project->description_en); ?></textarea>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-2 file-repeater">
                                                        <div class="col-md-9 mx-auto">
                                                            <div class="card-header">
                                                                <h4 class="card-title">Basic File Input</h4>
                                                            </div>
                                                            <div class="card-block">
                                                                <div class="card-body">
                                                                    <fieldset class="form-group">
                                                                        <div class="custom-file">
                                                                            <input name="gallery[]" type="file" class="custom-file-input" id="inputGroupFile02" multiple="multiple">
                                                                            <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Choose file</label>
                                                                        </div>
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <fieldset>
                                                                <label>SEO - Google Keywords KA</label>
                                                                <div class="form-group">
                                                                    <div id="keywordsKa" class="edit-on-delete form-control tagging" data-tags-input-name="keywords_ka">

                                                                        <?php $__currentLoopData = explode('|',$project->keywords_ka); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyword): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <div class="tag">
                                                                                <span>#</span>
                                                                                <?php echo e($keyword); ?>

                                                                                <input type="hidden" name="keywords_ka[]" value="<?php echo e($keyword); ?>">
                                                                                <a role="button" class="tag-i">×</a>
                                                                            </div>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <input class="type-zone" contenteditable="true">
                                                                    </div>
                                                                    <p class="text-muted mt-1">You can write some keywords for SEO</p>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <fieldset>
                                                                <label>SEO - Google Keywords EN</label>
                                                                <div class="form-group">
                                                                    <div id="keywordsEn" class="edit-on-delete form-control tagging" data-tags-input-name="keywords_en">
                                                                        <?php $__currentLoopData = explode('|',$project->keywords_en); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyword): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <div class="tag">
                                                                                <span>#</span>
                                                                                <?php echo e($keyword); ?> ,
                                                                                <input type="hidden" name="keywords_en[]" value="<?php echo e($keyword); ?>">
                                                                                <a role="button" class="tag-i">×</a>
                                                                            </div>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                            <input class="type-zone" contenteditable="true">
                                                                    </div>
                                                                    <p class="text-muted mt-1">You can write some keywords for SEO</p>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-9 mx-auto">
                                                            <input name="status" type="checkbox" id="switcheryColor4" class="switchery" data-color="success" <?php echo e($project->status?'checked':''); ?> />
                                                            <label for="switcheryColor4" class="card-title ml-1">Published</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-actions">

                                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Gallery Images</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-h font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <div style='float: left; width: 100%;'>
                                                <ul id="sortable" >
                                                    <?php
                                                        $gallery = json_decode($project->gallery)?json_decode($project->gallery):[];
                                                    ?>
                                                    <?php $__currentLoopData = $gallery; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $image): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                     <li style="position: relative" class="ui-state-default" id="image_" data-url="<?php echo e($image); ?>">
                                                         <img  src="/uploads/<?php echo e($image); ?>" title="" >
                                                         <button onClick="delete_row(this)" style="position: absolute;top: 0;right: 0;" type="button" class="btn btn-icon btn-danger mr-1 waves-effect waves-light"><i class="ft-x"></i></button>
                                                     </li>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                                </ul>
                                            </div>
                                            <div style="clear: both; margin-top: 20px;">
                                                <button class="btn btn-primary" type="button" id="savePosition">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/material-vendors.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/bootstrap-switch.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/vendors/css/forms/toggle/switchery.min.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/plugins/forms/switch.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/fonts/simple-line-icons/style.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('app-assets/css/core/colors/palette-switch.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('app-assets/vendors/css/forms/tags/tagging.css')); ?>">

    <style>
        #sortable {
            list-style-type: none;
            margin: 0;
            padding: 0;
            width: 90%;
        }
        #sortable li {
            margin: 3px 3px 3px 0;
            padding: 1px;
            float: left;
            border: 0;
            background: none;
        }
        #sortable li img{
            width: 180px;
            height: 140px;
        }
    </style>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer'); ?>
    <script src="<?php echo e(asset('tinymce/tinymce.js')); ?>"></script>
    <script>
        tinymce.init({
            selector:'textarea#tiny_en',
            height:500,
            plugins: "paste",
            paste_as_text: true,
        });
        tinymce.init({
            selector:'textarea#tiny_ka',
            height:500,
            plugins: "paste",
            paste_as_text: true,


        });


    </script>


    <!-- BEGIN: Page Vendor JS-->
    <script src="<?php echo e(asset('app-assets/vendors/js/extensions/dropzone.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/ui/prism.min.js')); ?>"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?php echo e(asset('app-assets/js/core/app-menu.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/core/app.js')); ?>"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?php echo e(asset('app-assets/js/scripts/pages/material-app.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/extensions/dropzone.js')); ?>"></script>
    <!-- END: Page JS-->
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-switch.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/switchery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/toggle/bootstrap-checkbox.min.js')); ?>"></script>

    <script src="<?php echo e(asset('app-assets/js/scripts/pages/material-app.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/switch.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/form-repeater.js')); ?>"></script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){

            // Initialize sortable
            $( "#sortable" ).sortable();

            // Save order
            $('#savePosition').click(function(){
                var image_arr = [];
                // get image ids order
                $('#sortable li').each(function(){
                    var img = $(this).attr('data-url');
                    image_arr.push(img);
                });

                // AJAX request
                $.ajax({
                    url: "<?php echo e(route('admin::updateGallery')); ?>",
                    type: 'POST',
                    data: {"gallery":image_arr, "_token": "<?php echo e(csrf_token()); ?>", "id": <?php echo e($project->id); ?>,  },
                    success: function(response){
                        console.log(response);
                        alert('Gallery Updated Successfully');
                    }
                });
            });
        });

        function delete_row(mine)
        {
            mine.parentElement.remove();
        }
    </script>


    <script src="<?php echo e(asset('app-assets/vendors/js/forms/tags/tagging.min.js')); ?>"></script>
    <script src="<?php echo e(asset('app-assets/js/scripts/forms/tags/tagging.js')); ?>"></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/admin/pages/projects/edit.blade.php ENDPATH**/ ?>