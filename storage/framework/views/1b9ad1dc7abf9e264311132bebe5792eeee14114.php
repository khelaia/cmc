<?php $__env->startSection('content'); ?>
    <div class="app-content content">
        <div class="content-header row">
        </div>
        <div class="content-wrapper">
            <div class="content-header row mb-1">
                <div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Career</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/admin">Admin</a>
                                </li>
                                <li class="breadcrumb-item"><a href="/admin/careers">Career</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 mb-2 text-right">
                    <a class="btn btn-primary waves-effect waves-light" href="/admin/careers/create">Add new</a>
                </div>
            </div>
            <div class="content-body">
                <!-- Default styling start -->
                <div class="row" id="default">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Default styling</h4>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard ">
                                    <p class="card-text"></p>
                                </div>
                                <?php if($message = Session::get('success')): ?>
                                    <div class="alert alert-success">
                                        <p><?php echo e($message); ?></p>
                                    </div>
                                <?php endif; ?>
                                <div class="table-responsive">
                                    <table class="table mb-0" id="formTable" data-sortable>
                                        <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Title</th>
                                            <th>Deadline</th>
                                            <th>Description</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="sortThis">
                                        <?php $__currentLoopData = $careers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $career): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr data-index="<?php echo e($career->id); ?>" data-position="<?php echo e($career->position); ?>">
                                                <td><?php echo e($career->id); ?></td>
                                                <td><?php echo e($career->title_ka); ?></td>
                                                <td><?php echo e($career->deadline); ?></td>
                                                <td><?php echo e($career->description_ka); ?></td>
                                                <td class="text-right">
                                                    <form action="<?php echo e(route('admin::careers.destroy',$career->id)); ?>" method="POST">
                                                        <?php echo csrf_field(); ?>
                                                        <?php echo method_field('DELETE'); ?>
                                                        <div class="btn-group arrow-right" role="group" aria-label="Basic example">
                                                            <a type="button" class="btn btn-primary" href="<?php echo e(route('admin::careers.edit',$career->id)); ?>">Edit</a>
                                                            <button type="submit" class="btn btn-danger">Remove</button>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Default styling end -->

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('position-update'); ?>
    <script type="text/javascript">
        function saveNewPositions() {
            var positions = [];
            $('.updated').each(function () {
                positions.push([$(this).attr('data-index'), $(this).attr('data-position')]);
                $(this).removeClass('updated');
            });

            $.ajax({
                url: '<?php echo e(route('admin::position_update_careers')); ?>',
                method: 'PUT',
                dataType: 'text',
                data: {
                    updated: 1,
                    positions: positions,
                    _token: '<?php echo e(csrf_token()); ?>'
                }, success: function (response) {
                    console.log(response);
                },error: function (data, textStatus, errorThrown) {
                    console.log(data);

                },
            });
        }
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/admin/pages/careers/index.blade.php ENDPATH**/ ?>