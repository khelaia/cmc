<?php $__env->startSection('content'); ?>
    <div id="main-page"></div>

    <div class="main_container">


        <?php echo $__env->make('user.includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


        <div class="header_blur">
            <img src="/assets/images/secondblur.png" alt="">
        </div>
        <div class="services_blue">
            <img src="/assets/images/dots.png" alt="">
        </div>
        <div class="swiper-container2">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="top_slider"
                         style="background-image: radial-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .2)), url(/assets/images/mainbg.png)">
                        <div class="slider_intro">
                            <h1>TBC New
                                Main Office</h1>
                            <span>Tbilisi, Amashukeli street</span>
                            <span>Tbc group</span>
                            <a href="#">view project</a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="top_slider"
                         style="background-image: radial-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .2)), url(/assets/images/mainbg.png)">
                        <div class="slider_intro">
                            <h1>TBC New
                                Main Office</h1>
                            <span>Tbilisi, Amashukeli street</span>
                            <span>Tbc group</span>
                            <a href="#">view project</a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="top_slider"
                         style="background-image: radial-gradient(rgba(0, 0, 0, .2), rgba(0, 0, 0, .2)), url(/assets/images/mainbg.png)">
                        <div class="slider_intro">
                            <h1>TBC New
                                Main Office</h1>
                            <span>Tbilisi, Amashukeli street</span>
                            <span>Tbc group</span>
                            <a href="#">view project</a>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper_arrows"></div>
            <!-- Add Arrows -->
            <div class="arrows">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>

    <div class="services_background">
        <div class="services_container">
            <div class="services_title">
                <h2>SERVICES</h2>
                <img src="/assets/images/servicebg.png" alt="">
            </div>
            <div class="services_column">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service_in_container">
                                <div class="services_in_title">
                                    <img src="/assets/images/service1.png" alt="">
                                    <h2>Construction
                                        Management</h2>
                                </div>
                                <div class="service_in_info">
                                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="services_section">
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
                <div class="services_block">
                    <img src="/assets/images/service.png" alt="">
                    <span>CONSTRUCTION MANAGEMENT</span>
                </div>
            </div> -->
        </div>
    </div>

    <div class="projects_container">
        <img src="/assets/images/projectbg.png" alt="">
        <div class="titles_container" >
            <div class="projects_title">
                <h2>PROJECTS</h2>
                <img src="/assets/images/projectsbg.png" alt="">
            </div>
            <div id="options">
                <div id="grid-filter" class="projects_filters">

                    <button id="all" class="button is-checked" data-filter="*">ALL</button>
                    <button id="one" class="button" data-filter="ongoing">Ongoing</button>
                    <button id="two" class="button" data-filter="completed">Complited</button>

                </div>
            </div>

            <div class="projects_full">
                <span><a href="">See all projects</a></span>
                <a href=""><img src="/assets/images/seeallbg.png" alt=""></a>
            </div>
        </div>

        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">
            <div class="projects_slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="projects_main_inside">
                                <div class="projects_inside_top">
                                    <div class="pit_left">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pit_right">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project1.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>UZNADZE LIVING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="projects_inside_bottom">
                                    <div class="pib_left">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project2.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>PROSECUTOR’S OFFICE</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pib_right">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project3.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>MERCEDES SHOPING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div id="divs" class="projects_box hideme">
                                <div class="projects_top">

                                </div>
                            </div> -->
                        </div>
                        <div class="swiper-slide">
                            <div class="projects_main_inside">
                                <div class="projects_inside_top">
                                    <div class="pit_left">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>MOJ TRAINING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pit_right">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project1.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>UZNADZE LIVING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="projects_inside_bottom">
                                    <div class="pib_left">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project2.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>PROSECUTOR’S OFFICE</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pib_right">
                                        <div class="projects_in_block" style="background-image: url(/assets/images/project3.png)">
                                            <img src="/assets/images/projectblur.png" alt="">
                                            <div class="projects_info">
                                                <h4>MERCEDES SHOPING CENTER</h4><span><img src="/assets/images/location.png" alt=""> TBILISI,
                                                    REPUBLIC OF
                                                    GEORGIA</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div id="divs" class="projects_box hideme">
                                <div class="projects_top">

                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clients_background">
        <div class="clients_top">
            <img src="/assets/images/clientbg.png" alt="">
        </div>
        <div class="clients_container">
            <div class="clients_title">
                <h2>CLIENTS</h2>
                <img src="/assets/images/clientsbg.png" alt="">
            </div>
            <div class="clients_section">
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
                <div class="clients_block">
                    <a href=""><img src="/assets/images/client.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>

    <div class="team_container">
        <img src="/assets/images/teamsbg.png" alt="">
        <div class="titles_container">
            <div class="projects_title">
                <h2>OUR TEAM</h2>
                <img src="/assets/images/teambg.png" alt="">
            </div>
            <div class="projects_full">
                <span><a href="">See all team</a></span>
                <a href=""><img src="/assets/images/seeallbg.png" alt=""></a>
            </div>
        </div>

        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay="1s">

            <div class="team_slider">
                <div class="swiper-container3">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="team_column">
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_column">
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_column">
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_column">
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_column">
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="team_column">
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="team_member" style="background-image: url(/assets/images/team.png)">
                                    <img src="/assets/images/memberbottom.png" alt="">
                                    <div class="team_info">
                                        <h4>Tamaz Tavadze</h4>
                                        <span>Founder</span>
                                    </div>
                                    <div class="member_hover" style="background-image: url(/assets/images/memberright.png)">
                                        <a href=""><img src="/assets/images/linkedin.png" alt=""></a>
                                    </div>
                                    <div class="read_more">
                                        <a href="">Read More <img src="/assets/images/readmore.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="team_bg"></div>
        <div class="team_bottom">
            <img src="/assets/images/teambottom.png" alt="">
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/user/pages/home.blade.php ENDPATH**/ ?>