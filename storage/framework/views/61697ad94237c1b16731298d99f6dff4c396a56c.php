<?php $__env->startSection('content'); ?>
    <div class="members_container">


        <?php echo $__env->make('user.includes.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="about_pages_blur">
            <img src="/assets/images/aboublur.png" alt="">
        </div>

        <div class="about_page_share">
            <span>SHARE</span>
            <div class="social_share">
                <a href=""><img src="/assets/images/youtube.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/fb.png" alt=""></a>
            </div>
            <div class="social_share">
                <a href=""><img src="/assets/images/in.png" alt=""></a>
            </div>
        </div>

    </div>



    <div class="page_fluid">
        <div class="inside_bg">
            <img src="/assets/images/projectbg.png" alt="">
        </div>
        <div class="inside_dots_top">
            <img src="/assets/images/insidedots.png" alt="">
        </div>
        <div class="container">
            <div class="title_tags">
                <a href="">Home</a>
                <a href="" class="title_arrows">Clients</a>
            </div>
            <div class="page_fluid_text">
                <h1>Clients</h1>
            </div>
        </div>
        <div class="clients_column">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="client_logo">
                            <img src="/assets/images/bog.png" alt="">
                            <a href="">
                                <div class="client_hover">
                                    <div class="hover_space">
                                        <img src="/assets/images/webright.png" alt="">
                                        <span>WEB SITE</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('user.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\User\Desktop\cmc\resources\views/user/pages/clients.blade.php ENDPATH**/ ?>