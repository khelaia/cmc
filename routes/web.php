<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Routing\UrlGenerator;
Route::get('/',function (){
    return redirect(app()->getLocale());
});

Route::get('/locale/{lang}', function ($lang){
    $id = explode('/',url()->previous());
    $id[3] = $lang;
    return redirect(implode('/',$id));
});

Route::group(['prefix'=>'{locale}','where' => ['locale' => 'ka|en|ru']],function (){
    Route::get('/', 'User\HomeController@index');
    Route::get('/projects','User\ProjectController@index');
    Route::get('/project/{slug}','User\ProjectController@show');
    Route::get('/clients','User\ClientController@index');
    Route::get('/services','User\ServiceController@index');
    Route::get('/our-team','User\StaffController@index');
    Route::get('/whoweare','User\AboutController@index');
    Route::get('/contact','User\ContactController@index');
    Route::get('/career','User\CareerController@index');
    Route::get('/gallery/image','User\GalleryController@image');
    Route::get('/gallery/video','User\GalleryController@video');
    Route::get('/gallery','User\GalleryController@index');
    Route::get('/contact','User\ContactController@index');
});
Route::post('/career','User\CareerController@store')->name('vacancies.store');





Route::group(['namespace' => 'Admin', 'as' => 'admin::', 'prefix' => 'admin'], function() {
    Route::resource('projects','ProjectController');
    Route::resource('staff','StaffController');
    Route::resource('services','ServiceController');
    Route::resource('categories','CategoryController');
    Route::resource('clients','ClientController');
    Route::resource('galleries','GalleryController');
    Route::resource('careers','CareerController');
    Route::resource('sliders','SliderController');
    Route::resource('pages','PageController');
    Route::get('menus','MenuController@index');
    Route::post('menus','MenuController@store')->name('menus.store');
    Route::delete('menus','MenuController@store')->name('menus.destroy');



    Route::get('vacancies','VacancyController@index')->name('vacancies.index');
    Route::get('about','AboutController@index');
    Route::put('about','AboutController@update')->name('about.update');
    Route::get('contact','ContactController@index');
    Route::put('contact','ContactController@update')->name('contact.update');

    Route::post('updateGallery','ProjectController@updateGallery')->name('updateGallery');

    //position updates
    Route::put('/position_update_pages','PageController@position_update')->name('position_update_pages');
    Route::put('/position_update_careers','CareerController@position_update')->name('position_update_careers');
    Route::put('/position_update_gallery','GalleryController@position_update')->name('position_update_gallery');
    Route::put('/position_update_clients','ClientController@position_update')->name('position_update_clients');
    Route::put('/position_update_services','ServiceController@position_update')->name('position_update_services');
    Route::put('/position_update_categories','CategoryController@position_update')->name('position_update_categories');
    Route::put('/position_update_staff','StaffController@position_update')->name('position_update_staff');
    Route::put('/position_update_projects','ProjectController@position_update')->name('position_update_projects');
    Route::put('/position_update_sliders','SliderController@position_update')->name('position_update_sliders');
    Route::put('/position_update_menus','MenuController@position_update')->name('position_update_menu');



    Route::get('logout',function (){
        Session::flush();
        Auth::logout();
        return back();
    });
});


Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Auth\LoginController@login');
Route::post('admin/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');


